package affichage.menu;

import affichage.jeux.*;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;

public class SaisirNom {

    public TextField champNom;
    public Button boutonAnnuler;
    private Integer typeJeu;
    private Integer nbJoueur;

    public SaisirNom(Integer typeJeu){
        this.typeJeu = typeJeu;
    }

    public SaisirNom(Integer typeJeu, Integer nbJoueur){
        this.typeJeu = typeJeu;
        this.nbJoueur = nbJoueur;
    }

    public void annulerNom(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.onCloseRequestProperty().setValue(e -> Platform.exit());
            stage.show();
            Stage s1 = (Stage)champNom.getScene().getWindow();
            s1.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void validerNom(MouseEvent mouseEvent) {
        switch (typeJeu){
            case 1:
                lancerSudoku();
                break;
            case 2:
                lancerLoto();
                break;
            case 3:
                lancerPoker();
                break;
            case 4:
                lancerNavale();
                break;
        }
    }

    private void lancerSudoku(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/DifficulteSudoku.fxml"));
            fxmlLoader.setControllerFactory(c -> {
                return new DifficulteSudoku(champNom.getText());
            });
            Parent root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Sudoku");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage) champNom.getScene().getWindow();
            s1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void lancerPoker(){
        if(nbJoueur <= 1) {
            try {
                PokerController.noms.add(champNom.getText());
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Poker.fxml"));
                fxmlLoader.setControllerFactory(c -> {
                    return new PokerController();
                });
                Parent root = (Parent) fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Poker");
                stage.setScene(scene);
                stage.show();
                Stage s = (Stage) champNom.getScene().getWindow();
                s.close();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        else {
            try {
                PokerController.noms.add(champNom.getText());
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Nom.fxml"));
                fxmlLoader.setControllerFactory(c -> {
                    return new SaisirNom(3, nbJoueur - 1);
                });
                Parent root = (Parent) fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Votre nom");
                stage.setScene(scene);
                stage.show();
                Stage s = (Stage) champNom.getScene().getWindow();
                s.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void lancerLoto(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/NbLoto.fxml"));
            fxmlLoader.setControllerFactory(c -> {
                return new NbLoto(champNom.getText());
            });
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Nombre de grilles");
            stage.setScene(scene);
            stage.show();

            Stage s1 = (Stage) champNom.getScene().getWindow();
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private void lancerNavale(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Bataille_navale.fxml"));
            fxmlLoader.setControllerFactory(c -> new NavaleController(champNom.getText()));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Bataille navale");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage) champNom.getScene().getWindow();
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
