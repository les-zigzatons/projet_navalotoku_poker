package affichage.menu;

import affichage.jeux.LotoController;
import affichage.jeux.NavaleJeuController;
import affichage.jeux.SudokuController;
import common.Joueur;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class MenuController {

    public AnchorPane fenetrePrincipal;
    public AnchorPane menuChoix;

    /**
     * Créer une nouvelle partie
     */
    public void nouvellePartie() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.onCloseRequestProperty().setValue(e -> Platform.exit());
            stage.show();
            Stage s1 = (Stage)fenetrePrincipal.getScene().getWindow();
            s1.close();
            Menu.app.setJoueur(new Joueur());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Charge une partie
     */
    public void chargerPartie() {
        try {
            Menu.app.setJoueur(new Joueur());
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Charger...");
            Stage choose = (Stage) fenetrePrincipal.getScene().getWindow();
            File file = fileChooser.showOpenDialog(choose);
            Joueur j = Menu.app.getJoueur();
            j.charger(file);
            Menu.app.setJoueur(j);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.onCloseRequestProperty().setValue(e -> Platform.exit());
            stage.show();
            Stage s = (Stage) fenetrePrincipal.getScene().getWindow();
            s.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Affiche le leadboard
     */
    public void leaderboard() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Leaderboard.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Tableau des scores");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage)menuChoix.getScene().getWindow();
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Quitte l'application
     */
    public void quitter() {
        Platform.exit();
    }

    /**
     * Sauvegare la partie
     */
    public void sauvegarder() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Sauvegarder...");
        Stage choose = (Stage)menuChoix.getScene().getWindow();
        File file = fileChooser.showSaveDialog(choose);
        Menu.app.getJoueur().sauvegarder(file);
    }

    /**
     * Quitte l'application
     */
    public void quitterMenuChoix() {
        Platform.exit();
    }

    /**
     * Démarre le Sudoku
     */
    public void startSudoku() {
        if(Menu.app.getJoueur().getSudoku() == null) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Nom.fxml"));
                fxmlLoader.setControllerFactory(c -> new SaisirNom(1));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Votre nom");
                stage.setScene(scene);
                stage.show();
                Stage s1 = (Stage)menuChoix.getScene().getWindow();
                s1.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
        else{
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Sudoku.fxml"));
                fxmlLoader.setControllerFactory(c -> new SudokuController(0, ""));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Sudoku");
                stage.setScene(scene);
                stage.show();
                Stage s1 = (Stage)menuChoix.getScene().getWindow();
                s1.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Démarre le loto
     */
    public void startLoto() {
        if(Menu.app.getJoueur().getLoto().isEmpty()){
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Nom.fxml"));
                fxmlLoader.setControllerFactory(c -> new SaisirNom(2));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Votre nom");
                stage.setScene(scene);
                stage.show();
                Stage s1 = (Stage)menuChoix.getScene().getWindow();
                s1.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
        else{
            Integer nb = Menu.app.getJoueur().getLoto().size();
            int taille = 0;
            int largeur = 0;
            try {
                for(int i = 0; i < nb; i++) {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Loto.fxml"));
                    fxmlLoader.setControllerFactory(c -> new LotoController(""));
                    Parent root = fxmlLoader.load();
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();
                    stage.setY(taille);
                    stage.setX(largeur);
                    taille += 240;
                    if(taille%720 == 0){
                        taille = 0;
                        largeur += 600;
                    }
                    LotoController controller = fxmlLoader.getController();
                    stage.setResizable(false);
                    stage.setTitle("Loto " + (i+1));
                    stage.setScene(scene);
                    stage.setOnHidden(e -> { controller.onClose(); });
                    stage.show();
                }
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Loto_tirage.fxml"));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Tirage");
                stage.setScene(scene);
                stage.show();

                Stage s1 = (Stage)menuChoix.getScene().getWindow();
                s1.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    /**
     * Démarre le poker
     */
    public void startPoker() {
        if(Menu.app.getJoueur().getPoker() == null) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/NbPoker.fxml"));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Nombre de joueur");
                stage.setScene(scene);
                stage.show();

                Stage s1 = (Stage) menuChoix.getScene().getWindow();
                s1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/ContinuePoker.fxml"));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Continuer ?");
                stage.setScene(scene);
                stage.show();
                Stage s1 = (Stage) menuChoix.getScene().getWindow();
                s1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Démarre la bataille navale
     */
    public void startNavale() {
        try {
            if (Menu.app.getJoueur().getNavale() == null) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Nom.fxml"));
                fxmlLoader.setControllerFactory(c -> new SaisirNom(4));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Votre nom");
                stage.setScene(scene);
                stage.show();
                Stage s1 = (Stage)menuChoix.getScene().getWindow();
                s1.close();
            }
            else{
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Bataille_navale_jeu.fxml"));
                fxmlLoader.setControllerFactory(c -> new NavaleJeuController(Menu.app.getJoueur().getNavale()));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Bataille navale");
                stage.setScene(scene);
                stage.show();
                Stage s1 = (Stage) menuChoix.getScene().getWindow();
                s1.close();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


}
