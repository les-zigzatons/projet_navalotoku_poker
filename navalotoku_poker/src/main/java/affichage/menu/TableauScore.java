package affichage.menu;

import common.Pair;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class TableauScore {
    public AnchorPane fenetrePrincipal;
    public ListView listSudoku;
    public ListView listNavale;
    public ListView listLoto;
    public ListView listPoker;

    /**
     * Initialise le tableau des scores
     */
    @FXML
    private void initialize() {
        for(Pair p : Menu.app.getJoueur().getLeaderboard().getScoresSudoku()){
            listSudoku.getItems().add(p);
        }
        for(Pair p : Menu.app.getJoueur().getLeaderboard().getScoresLoto()){
            listLoto.getItems().add(p);
        }
        for(Pair p : Menu.app.getJoueur().getLeaderboard().getScoresNavale()){
            listNavale.getItems().add(p);
        }
        for(Pair p : Menu.app.getJoueur().getLeaderboard().getScoresPoker()){
            listPoker.getItems().add(p);
        }
    }

    /**
     * Retourne au menu
     */
    public void retourMenu() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.onCloseRequestProperty().setValue(e -> Platform.exit());
            stage.show();
            Stage s1 = (Stage)fenetrePrincipal.getScene().getWindow();
            s1.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
