package affichage.menu;


import common.Joueur;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;



public class Menu extends Application {

    public static Menu app;
    private Joueur joueur;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Menu/Menu.fxml"));

        Scene scene = new Scene(root, 700, 400);
        stage.setResizable(false);

        stage.setTitle("Navalotoku Poker");
        stage.setScene(scene);
        stage.show();
        app = this;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }
}
