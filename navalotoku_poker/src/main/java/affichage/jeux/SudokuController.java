package affichage.jeux;

import affichage.menu.Menu;
import common.Joueur;
import common.Leaderboard;
import jeux.sudoku.Sudoku;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

public class SudokuController {

    public GridPane grille;
    public Button bouton1;
    public Button bouton2;
    public Button bouton3;
    public Button bouton4;
    public Button bouton5;
    public Button bouton6;
    public Button bouton7;
    public Button bouton8;
    public Button bouton9;
    public Label coups;
    public Label time;

    /**
     * Modele du Sudoku
     */
    private Sudoku s;

    /**
     * Numéro sélectionner
     */
    private Integer currentNumber;

    /**
     * Nombre de coups effectué
     */
    private Integer nbCoups;

    /**
     * Temps passé sur le sudoku
     */
    private int temps;

    /**
     * Initialise l'affichage de la grille de Suldou
     */
    @FXML
    private void initialize(){
        currentNumber = null;
        if(nbCoups == null)
            nbCoups = 0;
        coups.setText(nbCoups.toString());

        Integer temp;
        int x = 0, y = 0;
        for(Node cases : grille.getChildren()){
            if(cases.getClass() == Label.class){
                temp = (Integer)s.getGrille().getCase(x, y).getVal();
                if(temp < 0){
                    temp = Math.abs(temp);
                    ((Label) cases).setTextFill(Color.BLUE);
                }
                if(temp != 0) {
                    ((Label) cases).setText(temp.toString());
                }
                y++;
                if(y%9 == 0){
                    x++;
                    y = 0;
                }
            }
        }
        verifDisabled();
    }

    /**
     * Démarre une nouvelle partie de Sudoku ou charge si une partie existe déjà
     * @param nbtrou Nombre de trou sur la grille de Sudoku
     * @param nom Nom du joueur
     */
    public SudokuController(int nbtrou, String nom){
        nbCoups = null;
        temps = 0;
        if(Menu.app.getJoueur().getSudoku() != null){
            chargerSudoku();
        }
        else {
            s = new Sudoku(nbtrou, nom);
        }
        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            temps++;
            time.setText(String.format("%02d", temps/3600) + ":" + String.format("%02d",(temps/60)%60) + ":" + String.format("%02d",temps%60));
        }),
                new KeyFrame(Duration.seconds(1))
        );
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }


    /**
     * Récupère le numéro sur lequel on a cliqué
     * @param mouseEvent Clique de souris
     */
    public void selectionnedButton(MouseEvent mouseEvent) {
        Button b = (Button) mouseEvent.getSource();
        this.currentNumber = Integer.parseInt(b.getId());
    }

    /**
     * Place un numéro sur l'endroit cliqué
     * @param mouseEvent Clique de souris
     */
    public void setNumber(MouseEvent mouseEvent) {
        if(currentNumber != null) {
            Label l = (Label) mouseEvent.getSource();
            int row;
            int column;
            try {
                row = GridPane.getRowIndex(l);
            } catch (NullPointerException e) {
                row = 0;
            }
            try {
                column = GridPane.getColumnIndex(l);
            } catch (NullPointerException e) {
                column = 0;
            }

            if (l.getTextFill() == Color.BLUE || l.getTextFill() == Color.RED || l.getText().equals("")) {
                if (currentNumber != 0) {
                    if(s.verifNumero(row, column, currentNumber)){
                        l.setTextFill(Color.BLUE);
                    }
                    else{
                        l.setTextFill(Color.RED);
                    }
                    if(!l.getText().equals("")){
                       disableButton(false,Integer.parseInt(l.getText()));
                    }
                    s.insererNumero(-currentNumber, row, column);
                    l.setText(currentNumber.toString());

                    if (s.countNumber(currentNumber) >= s.getGrille().tailleX()) {
                        disableButton(true, currentNumber);
                        currentNumber = null;
                        if(bouton9.isDisable() && bouton8.isDisable() && bouton7.isDisable() && bouton6.isDisable() && bouton5.isDisable() &&
                                bouton4.isDisable() && bouton3.isDisable() && bouton2.isDisable() && bouton1.isDisable()){
                            verifVictoire();
                        }
                    }
                } else {
                    l.setText("");
                    disableButton(false,Math.abs((Integer) s.getGrille().getCase(row, column).getVal()));
                    s.insererNumero(currentNumber, row, column);
                }
                nbCoups++;
                coups.setText(nbCoups.toString());
            }
        }
    }

    /**
     * Met la gomme
     */
    public void setGomme() {
        currentNumber = 0;
    }

    /**
     * Retourne au menu et sauvegarde la partie
     */
    public void retourMenu() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage)grille.getScene().getWindow();
            s.setNbCoups(nbCoups);
            s.setTemps(temps);
            Joueur j = Menu.app.getJoueur();
            j.setSudoku(s);
            Menu.app.setJoueur(j);
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Charge la partie de Sudoku
     */
    private void chargerSudoku(){
        s = Menu.app.getJoueur().getSudoku();
        temps = s.getTemps();
        nbCoups = s.getNbCoups();
    }

    /**
     * Vérifie si la grille est terminé, si oui on sauvegarde le score du joueur
     */
    private void verifVictoire(){
        boolean verif = true;
        for(int x = 0 ; x < 9; x++){
            for(int y = 0; y < 9; y++){
                if((Integer)s.getGrille().getCase(x, y).getVal() <= 0 && !s.verifNumero(x, y, Math.abs((Integer)s.getGrille().getCase(x, y).getVal()))){
                    verif = false;
                }
            }
        }
        if(verif){
            int points;
            if(nbCoups < 35){
                points = 200 - temps/60;
            }
            else{
                points = (200 - temps/60)/(nbCoups - 35);
            }
            Leaderboard l = Menu.app.getJoueur().getLeaderboard();
            l.ajouterScoreSudoku(s.getNom(), points);
            Menu.app.getJoueur().setLeaderboard(l);
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.setTitle("Selection du jeu ?");
                stage.setScene(scene);
                stage.show();
                Stage s1 = (Stage)grille.getScene().getWindow();
                s.setNbCoups(nbCoups);
                s.setTemps(temps);
                Joueur j = Menu.app.getJoueur();
                j.setSudoku(null);
                Menu.app.setJoueur(j);
                s1.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Désactive un numéro s'il est présent 9 fois
     */
    private void verifDisabled(){
        for(int  i = 1; i <= 9 ; i++){
            if (s.countNumber(i) >= s.getGrille().tailleX()) {
                disableButton(true, i);
            }
        }
    }

    /**
     * Désactive ou réactive le bouton correspondant
     * @param etat Désactive ou active le bouton
     * @param number Numéro du bouton
     */
    private void disableButton(boolean etat, int number){
        switch (number) {
            case 1:
                bouton1.setDisable(etat);
                break;
            case 2:
                bouton2.setDisable(etat);
                break;
            case 3:
                bouton3.setDisable(etat);
                break;
            case 4:
                bouton4.setDisable(etat);
                break;
            case 5:
                bouton5.setDisable(etat);
                break;
            case 6:
                bouton6.setDisable(etat);
                break;
            case 7:
                bouton7.setDisable(etat);
                break;
            case 8:
                bouton8.setDisable(etat);
                break;
            case 9:
                bouton9.setDisable(etat);
                break;
        }
    }
}
