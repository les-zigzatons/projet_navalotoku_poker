package affichage.jeux;

import affichage.menu.Menu;
import common.Case;
import common.Joueur;
import common.Leaderboard;
import jeux.bataille_navale.Bateau;
import jeux.bataille_navale.Navale;
import jeux.bataille_navale.TypeCellule;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class NavaleJeuController {

    @FXML
    public GridPane gridJoueur;
    public GridPane gridAdversaire;

    /**
     * Modele de la bataille navale
     */
    private Navale n;

    /**
     * Initialise les grilles de deux joueurs
     */
    @FXML
    private void initialize() {
        remplirGrilleJoueur();
        remplirGrilleAdversaire();
    }

    /**
     * Constructeur de NavaleEnJeuController
     * @param n Modele du jeu
     */
    public NavaleJeuController(Navale n) {
        this.n = n;
    }

    /**
     * Permet de retourner au menu des jeux
     */
    public void retourMenu(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage)gridJoueur.getScene().getWindow();
            Joueur j = Menu.app.getJoueur();
            j.setNavale(n);
            Menu.app.setJoueur(j);
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     *Permet de remplir la grille et le gridpane
     */
    public void remplirGrilleJoueur() {
        Case cas;
        Label l;
        Background bg;
        for (int i = 0; i < n.getGrilleJ1().tailleX(); i++) {
            for (int j = 0; j < n.getGrilleJ1().tailleY(); j++) {
                cas = n.getGrilleJ1().getCase(i, j);
                l = getNodeByRowColumnIndexJoueur(i, j);
                ImageView imageView = new ImageView();
                imageView.setImage(new Image(String.valueOf(getClass().getResource("/images/croix.png"))));
                if (cas.getVal() == TypeCellule.Bateau) {
                    l.setStyle("-fx-background-color:POWDERBLUE");
                }
                else if(cas.getVal() == TypeCellule.Rate){
                    gridJoueur.add(imageView, j, i);
                }
                else if(cas.getVal() == TypeCellule.Touche){
                    l.setStyle("-fx-background-color:ORANGE");
                    gridJoueur.add(imageView, j, i);
                }
                else if(cas.getVal() == TypeCellule.Coule){
                    l.setStyle("-fx-background-color:RED");
                }
            }
        }
    }

    /**
     *Permet de remplir la grille et le gridpane
     */
    public void remplirGrilleAdversaire() {
        Case cas;
        Label l;
        Background bg;
        for (int i = 0; i < n.getGrilleJ2().tailleX(); i++) {
            for (int j = 0; j < n.getGrilleJ2().tailleY(); j++) {
                cas = n.getGrilleJ2().getCase(i, j);
                l = getNodeByRowColumnIndexAdversaire(i, j);
                ImageView imageView = new ImageView();
                imageView.setImage(new Image(String.valueOf(getClass().getResource("/images/croix.png"))));
                if(cas.getVal() == TypeCellule.Touche){
                    l.setStyle("-fx-background-color:ORANGE");
                    gridAdversaire.add(imageView, j, i);
                }
                else if(cas.getVal() == TypeCellule.Rate){
                    gridAdversaire.add(imageView, j, i);
                }
                else if(cas.getVal() == TypeCellule.Coule){
                    l.setStyle("-fx-background-color:RED");
                }
            }
        }
    }

    /**
     * Renvoie le node présent à une ligne et une colonne données
     * @param row Ligne
     * @param column Colonne
     * @return Noeud aux coordonnées données
     */
    public Label getNodeByRowColumnIndexJoueur(final int row, final int column) {
        int temprow, tempcolumn;
        for (Node node : gridJoueur.getChildren()) {
            if((node instanceof Label)) {
                if(GridPane.getRowIndex(node) == null){
                    temprow = 0;
                }
                else {
                    temprow = GridPane.getRowIndex(node);
                }
                if(GridPane.getColumnIndex(node) == null){
                    tempcolumn = 0;
                }
                else{
                    tempcolumn = GridPane.getColumnIndex(node);
                }
                if (temprow == row && tempcolumn == column) {
                    return (Label)node;
                }
            }
        }
        return null;
    }

    /**
     * Renvoie le node présent à une ligne et une colonne données pour l'adversaire
     * @param row Ligne
     * @param column Colonne
     * @return Noeud aux coordonnées données
     */
    public Label getNodeByRowColumnIndexAdversaire(final int row, final int column) {
        int temprow, tempcolumn;
        for (Node node : gridAdversaire.getChildren()) {
            if((node instanceof Label)) {
                if(GridPane.getRowIndex(node) == null){
                    temprow = 0;
                }
                else {
                    temprow = GridPane.getRowIndex(node);
                }
                if(GridPane.getColumnIndex(node) == null){
                    tempcolumn = 0;
                }
                else{
                    tempcolumn = GridPane.getColumnIndex(node);
                }
                if (temprow == row && tempcolumn == column) {
                    return (Label)node;
                }
            }
        }
        return null;
    }

    /**
     * Calcul les points de la partie et fini la partie
     */
    private void finPartie(){
        int points = 500;
        if(n.verifVictoireJ1()){
            for(Bateau bateau : n.getBateauxJ1()){
                if(bateau.isCoule()){
                    points -= 100;
                }
            }
        }
        Leaderboard l = Menu.app.getJoueur().getLeaderboard();
        l.ajouterScoreNavale(n.getNom(), points);
        Menu.app.getJoueur().setLeaderboard(l);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage)gridJoueur.getScene().getWindow();
            Joueur j = Menu.app.getJoueur();
            j.setNavale(null);
            Menu.app.setJoueur(j);
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Tire sur la case cliquée
     * @param mouseEvent Clique de souris
     */
    public void tirer(MouseEvent mouseEvent){
        Label l = (Label)mouseEvent.getSource();
        int row,column;
        try {
            row = GridPane.getRowIndex(l);
        } catch (NullPointerException e) {
            row = 0;
        }
        try {
            column = GridPane.getColumnIndex(l);
        } catch (NullPointerException e) {
            column = 0;
        }
        n.tireJ2(row, column);
        n.tireJ1Rand();
        remplirGrilleAdversaire();
        remplirGrilleJoueur();
        if(n.verifVictoireJ1() || n.verifVictoireJ2())
            finPartie();
    }
}