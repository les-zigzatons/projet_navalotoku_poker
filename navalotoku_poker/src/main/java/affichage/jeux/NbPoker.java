package affichage.jeux;

import affichage.menu.SaisirNom;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.ArrayList;

public class NbPoker {
    public ChoiceBox choicebox;

    /**
     * Initialise le menu déroulant de la popup
     */
    @FXML
    private void initialize(){
        Integer[] val = new Integer[4];
        for(int i = 2; i <= 5 ; i++){
            val[i-2] = i;
        }
        choicebox.getItems().addAll(val);
        choicebox.setValue(val[0]);
    }

    /**
     * Annule la création d'une partie de poker
     */
    public void annulerPoker() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Menu");
            stage.setScene(scene);
            stage.onCloseRequestProperty().setValue(e -> Platform.exit());
            stage.show();
            Stage s = (Stage) choicebox.getScene().getWindow();
            s.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Valide la création d'une partie de poker
     */
    public void validerPoker() {
        try {
            PokerController.noms = new ArrayList<>();
            Integer nb = (Integer)this.choicebox.getValue();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Nom.fxml"));
            fxmlLoader.setControllerFactory(c -> new SaisirNom(3,nb));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Votre nom");
            stage.setScene(scene);
            stage.onCloseRequestProperty().setValue(e -> Platform.exit());
            stage.show();
            Stage s = (Stage) choicebox.getScene().getWindow();
            s.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}
