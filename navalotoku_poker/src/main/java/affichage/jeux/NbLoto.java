package affichage.jeux;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;

public class NbLoto {
    public ChoiceBox choicebox;
    private String nom;

    /**
     * Initialise le menu déroulant avec 9 numéors
     */
    @FXML
    private void initialize(){
        Integer[] val = new Integer[9];
        for(int i = 1; i <= 9 ; i++){
            val[i-1] = i;
        }
        choicebox.getItems().addAll(val);
        choicebox.setValue(val[0]);
    }

    /**
     * Défini le nom du joueur
     * @param nom Nom du joueur
     */
    public NbLoto(String nom){
        this.nom = nom;
    }

    /**
     * Annule la création de la partie
     */
    public void annulerNbLoto() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.onCloseRequestProperty().setValue(e -> Platform.exit());
            stage.show();
            Stage s = (Stage) choicebox.getScene().getWindow();
            s.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Valide la création de la partie
     */
    public void validerNbLoto() {
        Integer nb = (Integer)this.choicebox.getValue();
        int taille = 0;
        int largeur = 0;
        try {
            for(int i = 0; i < nb; i++) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Loto.fxml"));
                fxmlLoader.setControllerFactory(c -> new LotoController(this.nom));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setY(taille);
                stage.setX(largeur);
                taille += 240;
                if(taille%720 == 0){
                    taille = 0;
                    largeur += 600;
                }
                LotoController controller = fxmlLoader.getController();
                stage.setResizable(false);
                stage.setTitle("Loto " + (i+1));
                stage.setScene(scene);
                stage.setOnHidden(e -> { controller.onClose(); });
                stage.show();
            }
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Loto_tirage.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Tirage");
            stage.setScene(scene);
            stage.show();

            Stage s1 = (Stage)choicebox.getScene().getWindow();
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
