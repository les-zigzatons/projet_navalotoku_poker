package affichage.jeux;

import affichage.menu.Menu;
import common.Joueur;
import common.Leaderboard;
import jeux.poker.Poker;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ContinuePoker {

    /**
     * Fenètre de la popup
     */
    public AnchorPane fenetre;

    /**
     * Sauvegarde les scores de la partie et relance une nouvelle partie de Poker
     */
    public void recommencer() {
        try {
            Poker jeu = Menu.app.getJoueur().getPoker();
            Leaderboard l = Menu.app.getJoueur().getLeaderboard();
            for(int i = 0; i < jeu.nbJoueurs(); i++){
                l.ajouterScorePoker(jeu.getMain(i).getNom(), jeu.getMain(i).getJetons());
            }
            Menu.app.getJoueur().setLeaderboard(l);
            Joueur j = Menu.app.getJoueur();
            j.setPoker(null);
            Menu.app.setJoueur(j);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/NbPoker.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Nombre de joueur");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage) fenetre.getScene().getWindow();
            s1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Relance la partie de poker qui à été sauvegardée
     */
    public void continuer() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Poker.fxml"));
            fxmlLoader.setControllerFactory(c -> new PokerController());
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Nombre de joueur");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage) fenetre.getScene().getWindow();
            s1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
