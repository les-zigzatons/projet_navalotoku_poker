package affichage.jeux;

import affichage.menu.Menu;
import common.Joueur;
import common.Leaderboard;
import jeux.poker.Mains;
import jeux.poker.Poker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public class PokerController {

    public HBox joueur1;
    public VBox joueur2;
    public VBox joueur3;
    public HBox joueur4;
    public HBox joueur5;
    public Button echanger;
    public Button miser;
    public Button tapis;
    public Button suivre;
    public Spinner champmise;
    public Button boutonmise;
    public Label currentmise;
    public Label jetons;
    public ImageView retour;
    public Label label_retour;
    public Label victoire;
    public Label nomJoueur;

    /**
     * Modele du poker
     */
    private Poker jeu;

    /**
     * Joueur en train de jouer
     */
    private int currentJoueur;

    /**
     * Liste des noms des joueurs
     */
    public static List<String> noms;

    /**
     * Initialise la partie de poker
     */
    @FXML
    private void initialize() {
        jeu.premierTour();
        debutJeu();
    }

    /**
     * Créer une nouvelle partie de poker ou charge la partie de poker si elle existe
     */
    public PokerController(){
        if(Menu.app.getJoueur().getPoker() != null)
            this.jeu = Menu.app.getJoueur().getPoker();
        else
            this.jeu = new Poker(noms);
    }

    /**
     * Affiche les cartes des joueurs
     */
    private void loadImage(){
        joueur1.getChildren().forEach(e -> {
            e.setVisible(false);
        });
        joueur2.getChildren().forEach(e -> {
            e.setVisible(false);
        });
        joueur3.getChildren().forEach(e -> {
            e.setVisible(false);
        });
        joueur4.getChildren().forEach(e -> {
            e.setVisible(false);
        });
        joueur5.getChildren().forEach(e -> {
            e.setVisible(false);
        });
        for(int i = 0; i < jeu.nbJoueurs() ; i++) {
            Pane joueur;
            switch (i) {
                case 1:
                    joueur = joueur2;
                    break;
                case 2:
                    joueur = joueur3;
                    break;
                case 3:
                    joueur = joueur4;
                    break;
                case 4:
                    joueur = joueur5;
                    break;
                default:
                    joueur = joueur1;
            }
            int j = 0;
            String t;
            for (Node image : joueur.getChildren()) {
                if (image.getClass() == ImageView.class) {
                    t = getClass().getResource("/images/cartes/Red_back.jpg").toString();
                    image.setVisible(true);
                    ((ImageView) image).setImage(new Image(t));
                    if (joueur.getClass() == VBox.class) {
                        image.setRotate(90);
                    }
                    ((ImageView) image).setSmooth(true);
                }
                j++;
            }
        }
    }

    /**
     * Affiche la main du joueur courant
     * @param joueur Joueur courant
     */
    private void loadMain(int joueur){
        Mains m = jeu.getMain(joueur);
        nomJoueur.setText("Joueur " + m.getNom());
        String t;
        int i = 0;
        for(Node image : joueur1.getChildren()) {
            t = getClass().getResource("/images/cartes/" + m.getMain()[i].toString() + ".jpg").toString();
            ((ImageView) image).setImage(new Image(t));
            ((ImageView) image).setSmooth(true);
            i++;
        }
    }

    /**
     * Permet de choisir une carte que l'on veut échanger
     * @param mouseEvent Clique de souris
     */
    public void choisirCarte(MouseEvent mouseEvent) {
        if(echanger.getText().equals("Echanger")) {
            ImageView image = (ImageView) mouseEvent.getSource();
            if (image.getTranslateY() > -20)
                image.setTranslateY(-20);
            else
                image.setTranslateY(0);
        }

    }

    /**
     * Echange les cartes et affiche la suite du jeu après l'échange des cartes
     */
    public void echangerCarte() {
        if(echanger.getText().equals("Finir")){
            Leaderboard l = Menu.app.getJoueur().getLeaderboard();
            for(int i = 0; i < jeu.nbJoueurs(); i++){
                l.ajouterScorePoker(jeu.getMain(i).getNom(), jeu.getMain(i).getJetons());
            }
            Menu.app.getJoueur().setLeaderboard(l);
            Joueur j = Menu.app.getJoueur();
            j.setPoker(null);
            Menu.app.setJoueur(j);
            retourMenu();
        }
        else if(echanger.getText().equals("Partie suivante")){
            debutJeu();
        }
        else if(echanger.getText().equals("Se coucher")){
            jeu.getMain(currentJoueur).seCoucher();
            joueurSuivant();
        }
        else if(echanger.getText().equals("Echanger") || !jeu.isPremiertour()) {
            int i = 0;
            for (Node n : joueur1.getChildren()) {
                if (n.getClass() == ImageView.class && n.getTranslateY() == -20) {
                    jeu.getMain(currentJoueur).echangerCarte(jeu.getPaquet(), i);
                    n.setTranslateY(currentJoueur);
                }
                i++;
            }
            jeu.getMain(currentJoueur).rangerMain();
            loadMain(currentJoueur);
            echanger.setText("Se coucher");
            if(currentJoueur != 0 || !jeu.isPremiertour()) {
                suivre.setVisible(true);
                if(getJoueurPrecedent().getMise() == jeu.getMain(currentJoueur).getMise()) {
                    suivre.setText("Check");
                }
                else{
                    suivre.setText("Suivre");
                }
            }
            tapis.setVisible(true);
            if(getJoueurPrecedent().getMise() < jeu.getMain(currentJoueur).getJetons()){
                miser.setVisible(true);
                if(currentJoueur == 0){
                    miser.setText("Miser");
                }
                else{
                    miser.setText("Relancer");
                }
            }

            this.jetons.setText(Integer.toString(jeu.getMain(currentJoueur).getJetons()));
        }
        else if(echanger.getText().equals("Suivant")){
            loadMain(currentJoueur);
            nomJoueur.setText("Joueur " + jeu.getMain(currentJoueur).getNom());
            echanger.setText("Echanger");
            this.jetons.setText(Integer.toString(jeu.getMain(currentJoueur).getJetons()));
        }
    }

    /**
     * Affiche le selecteur de mise
     */
    public void afficherMise() {
        int min = getJoueurPrecedent().getMise() + 1;
        suivre.setVisible(false);
        tapis.setVisible(false);
        miser.setVisible(false);
        echanger.setVisible(false);
        champmise.setVisible(true);
        boutonmise.setVisible(true);
        champmise.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(min, jeu.getMain(currentJoueur).getJetons()));
        champmise.getEditor().setTextFormatter(new TextFormatter<String>(change -> {
            final int oldLength = change.getControlText().length();
            int newLength = change.getControlNewText().length();
            change.setCaretPosition(newLength);
            change.setAnchor(newLength);

            if (newLength < oldLength) return change;
            try{
                Integer.parseInt(change.getText());
                return change;
            }
            catch (NumberFormatException e){
                return null;
            }
        }));
    }

    /**
     * Provoque le tapis du joueur courant
     */
    public void faireTapis() {
        int jetons = jeu.getMain(currentJoueur).getJetons();

        jeu.getMain(currentJoueur).setMise(jetons);
        jeu.getMain(currentJoueur).removeJetons(jetons);
        jeu.addMisetotal(jetons);
        currentmise.setText(Integer.toString(jeu.getMisetotal()));
        this.jetons.setText(Integer.toString(jeu.getMain(currentJoueur).getJetons()));
        joueurSuivant();
    }

    /**
     * Permet de suivre le joueur précedent
     */
    public void faireSuivre() {
        if(suivre.getText().equals("Suivre")) {
            int jetons = getJoueurPrecedent().getMise() - jeu.getMain(currentJoueur).getMise();
            jeu.getMain(currentJoueur).setMise(jetons);
            jeu.getMain(currentJoueur).removeJetons(jetons);
            jeu.addMisetotal(jetons);
            currentmise.setText(Integer.toString(jeu.getMisetotal()));
            this.jetons.setText(Integer.toString(jeu.getMain(currentJoueur).getJetons()));
        }
        joueurSuivant();

    }

    /**
     * Retourne les cartes du joueur courant et demande au joueur suivant de venir jouer
     */
    private void joueurSuivant(){
        String t;
        for(Node image : joueur1.getChildren()) {
            t = getClass().getResource("/images/cartes/Red_back.jpg").toString();
            ((ImageView) image).setImage(new Image(t));
            ((ImageView) image).setSmooth(true);
        }
        echanger.setText("Suivant");
        suivre.setVisible(false);
        tapis.setVisible(false);
        miser.setVisible(false);
        do{
            currentJoueur++;
            if(currentJoueur >= jeu.nbJoueurs() && jeu.isPremiertour()){
                currentJoueur = currentJoueur%jeu.nbJoueurs();
                jeu.secondTour();
            }
            else if(currentJoueur >= jeu.nbJoueurs() && !jeu.isPremiertour()){
                devoilerCarte();
                jeu.finJeu();
                echanger.setText("Partie suivante");
                if(jeu.nbJoueurs() < 2){
                    echanger.setText("Finir");
                }
                break;
            }

        }while (jeu.getMain(currentJoueur).isCoucher() || jeu.getMain(currentJoueur).getJetons() == 0);
    }

    /**
     * Permet de miser
     */
    public void faireMise() {
        int jetons = Integer.parseInt(champmise.getEditor().getText());
        jeu.getMain(currentJoueur).setMise(jetons);
        jeu.getMain(currentJoueur).removeJetons(jetons);
        jeu.addMisetotal(jeu.getMain(currentJoueur).getMise());
        currentmise.setText(Integer.toString(jeu.getMisetotal()));
        this.jetons.setText(Integer.toString(jeu.getMain(currentJoueur).getJetons()));
        champmise.setVisible(false);
        boutonmise.setVisible(false);
        echanger.setVisible(true);
        joueurSuivant();
    }

    /**
     * Récupère le joueur précédent
     * @return Main du joueur précédent
     */
    private Mains getJoueurPrecedent() {
        Mains precedent;
        int v = currentJoueur;
        do{
            if(v == 0){
                v = jeu.nbJoueurs() - 1;
            }
            else{
                v = v - 1;
            }
            precedent = jeu.getMain(v);
        }while(precedent.isCoucher());
        return precedent;
    }

    /**
     * Permet de début la partie
     */
    private void debutJeu(){
        miser.setVisible(false);
        tapis.setVisible(false);
        suivre.setVisible(false);
        label_retour.setVisible(false);
        retour.setVisible(false);
        victoire.setText("");
        echanger.setText("Echanger");
        loadImage();
        loadMain(0);
        currentJoueur = 0;
        jetons.setText(Integer.toString(jeu.getMain(currentJoueur).getJetons()));
    }

    /**
     * Permet de dévoiler toutes les cartes de tout les joueurs
     */
    private void devoilerCarte(){
        Mains joue;
        for(int i = 0; i < jeu.nbJoueurs() ; i++) {
            joue = jeu.getMain(i);
            Pane joueur;
            switch (i) {
                case 1:
                    joueur = joueur2;
                    break;
                case 2:
                    joueur = joueur3;
                    break;
                case 3:
                    joueur = joueur4;
                    break;
                case 4:
                    joueur = joueur5;
                    break;
                default:
                    joueur = joueur1;
            }
            int j = 0;
            String t;
            for (Node image : joueur.getChildren()) {
                if (image.getClass() == ImageView.class) {
                    t = getClass().getResource("/images/cartes/" + joue.getMain()[j].toString() + ".jpg").toString();
                    ((ImageView) image).setImage(new Image(t));
                    if (joueur.getClass() == VBox.class) {
                        image.setRotate(90);
                    }
                    ((ImageView) image).setSmooth(true);
                }
                j++;
            }
        }
        label_retour.setVisible(true);
        retour.setVisible(true);
        victoire.setText(jeu.getGagnant());
        nomJoueur.setText("");
    }

    /**
     * Permet de retourner au menu en sauvegardant la partie
     */
    public void retourMenu() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage)joueur1.getScene().getWindow();
            Joueur j = Menu.app.getJoueur();
            j.setPoker(jeu);
            Menu.app.setJoueur(j);
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
