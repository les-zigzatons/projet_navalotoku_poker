package affichage.jeux;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class DifficulteSudoku {

    /**
     * Fenètre de choix de la difficulté du sudoku
     */
    public AnchorPane fenetre;

    /**
     * Nom qui à été saisie
     */
    private String nom;

    /**
     * Permet de récupérer le nom qui à été saisie en utilisant le constructeur de la classe
     * @param nom Nom du joueur
     */
    public DifficulteSudoku(String nom){
        this.nom = nom;
    }

    /*
     * Choix de la difficulté facile
     */
    public void facile() {
        demarrerSudoku(20);
    }

    /**
     * Choix de la difficulté moyen
     */
    public void moyen() {
        demarrerSudoku(30);
    }

    /**
     * Choix de la difficulté difficile
     */
    public void difficile() {
        demarrerSudoku(40);
    }

    /**
     * permet de lancer le sudoku avec le nombre de trou pour le joueur selon le nombre de trou
     * @param nbtrou nombre de trou qu'il y aura dans le jeu
     */
    private void demarrerSudoku(int nbtrou){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Sudoku.fxml"));
            fxmlLoader.setControllerFactory(c -> new SudokuController(nbtrou, nom));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Sudoku");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage)fenetre.getScene().getWindow();
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
