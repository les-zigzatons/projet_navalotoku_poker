package affichage.jeux;

import affichage.menu.Menu;
import common.Joueur;
import jeux.loto.Loto;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class LotoController {
    /**
     * Grille de loto affiché
     */
    public GridPane grille;

    /**
     * Modele du loto
     */
    private Loto loto;

    /**
     * Créer un nouveau loto ou récupère la sauvegarde du loto s'il y en a une
     * @param nom Nom du joueur
     */
    public LotoController(String nom){
        if(Menu.app.getJoueur().getLoto().isEmpty()){
            loto = new Loto(nom);
        }
        else{
            loto = Menu.app.getJoueur().getLoto().get(0);
            Menu.app.getJoueur().getLoto().remove(0);
        }
    }

    /**
     * Initialise l'affichage des grilles de loto
     */
    @FXML
    private void initialize() {
        Label l;
        int x = 0, y = 0;
        Integer val;
        for(Node n : grille.getChildren()){
            if(n.getClass() == Label.class) {
                l = (Label) n;
                if ((Integer) loto.getGrille().getCase(x, y).getVal() == 0) {
                    l.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
                } else {
                    val = ((Integer) loto.getGrille().getCase(x, y).getVal());
                    if(val < 0)
                        displayCircle(l);
                    val = Math.abs(val);
                    l.setText(val.toString());

                }
                x++;
                if (x % 9 == 0) {
                    x = 0;
                    y++;
                }
            }
        }
    }


    /**
     * Vérifie si on a le droit de cocher le numéro
     * @param mouseEvent événement du clique de souris sur le numéro
     */
    public void checkNumero(MouseEvent mouseEvent) {
        int row, column;
        try {
            Label b = (Label) mouseEvent.getSource();
            if (Integer.parseInt(b.getText()) == LotoTirageController.getNumero()) {
                try {
                    row = GridPane.getRowIndex(b);
                } catch (NullPointerException e) {
                    row = 0;
                }
                try {
                    column = GridPane.getColumnIndex(b);
                } catch (NullPointerException e) {
                    column = 0;
                }
                loto.numeroTire(column, row);
                displayCircle(b);
                if(loto.verifVictoire()){
                    ((Stage)b.getScene().getWindow()).close();
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupére la partie en cours
     * @return La partie en cours
     */
    public Loto getLoto(){
        return this.loto;
    }

    /**
     * Sauvegarde la partie si on sort de jeu
     */
    public void onClose(){
        Joueur j = Menu.app.getJoueur();
        j.addLoto(loto);
        Menu.app.setJoueur(j);
    }

    /**
     * Affichage un cercle autour d'un numéro
     * @param b Noeud que l'on veut entourer
     */
    private void displayCircle(Node b){
        for(Node n : ((GridPane)b.getParent()).getChildren()){
            if(n.getClass() == Circle.class && GridPane.getColumnIndex(n) == GridPane.getColumnIndex(b) && GridPane.getRowIndex(n) == GridPane.getRowIndex(b)){
                n.setOpacity(1);
                n.setMouseTransparent(false);
            }
        }
    }

}
