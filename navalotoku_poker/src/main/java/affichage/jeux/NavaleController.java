package affichage.jeux;

import common.Case;
import jeux.bataille_navale.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


import java.io.IOException;

public class NavaleController {

    public Button boutonValider;
    public GridPane grid;
    public GridPane torpilleur;
    public GridPane contreTorpilleur1;
    public GridPane contreTorpilleur2;
    public GridPane croiseur;
    public GridPane porte_avion;
    private GridPane currentBoat;

    /**
     * Modele de la bataille navale
     */
    private Navale n;

    /**
     * Boolean pour savoir si on est en mode suppression ou non
     */
    private boolean supression;

    /**
     * Rempli la grille de case vide
     */
    @FXML
    private void initialize() {
        remplir();
    }

    /**
     * Constructeur de NavaleControlleur
     */
    public NavaleController(String nom) {
        this.n = new Navale(nom);
        this.supression = false;
    }

    /**
     * Permet de retourner au menu des jeux
     */
    public void retourMenu() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage)grid.getScene().getWindow();;
            s1.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     *Permet de remplir la grille et le gridpane
     */
    public void remplir() {
        Case cas;
        Label l;
        Background bg;
        for (int i = 0; i < n.getGrilleJ1().tailleX(); i++) {
            for (int j = 0; j < n.getGrilleJ1().tailleY(); j++) {
                cas = n.getGrilleJ1().getCase(i, j);
                l = getNodeByRowColumnIndex(i, j);
                if (cas.getVal() == TypeCellule.Bateau && l.getBackground() == null ) {
                    bg = currentBoat.getBackground();
                    l.setBackground(bg);
                }
                else if (cas.getVal() == TypeCellule.Vide && l.getBackground() != null) {
                    l.setBackground(null);
                }
            }
        }
        if(n.tousPlacer())
            boutonValider.setVisible(true);
        else
            boutonValider.setVisible(false);
    }

    /**
     * Renvoie le node présent à une ligne et une colonne données
     * @param row Ligne
     * @param column Colonne
     * @return Noeud aux coordonnées données
     */
    public Label getNodeByRowColumnIndex (final int row, final int column) {
        int temprow, tempcolumn;
        for (Node node : grid.getChildren()) {
            if((node instanceof Label)) {
                if(GridPane.getRowIndex(node) == null){
                    temprow = 0;
                }
                else {
                    temprow = GridPane.getRowIndex(node);
                }
                if(GridPane.getColumnIndex(node) == null){
                    tempcolumn = 0;
                }
                else{
                    tempcolumn = GridPane.getColumnIndex(node);
                }
                if (temprow == row && tempcolumn == column) {
                    return (Label)node;
                }
            }
        }
        return null;
    }

    /**
     * Click sur la gomme
     */
    public void toDeleteBoat(){
        this.supression = true;
    }

    /**
     * Pour supprimer un bateau du gridpane et de la grille
     * @param mouseEvent Clique de souris
     */
    public void deleteBoat(MouseEvent mouseEvent){
        Label l = (Label) mouseEvent.getSource();
        if (l.getBackground().equals(torpilleur.getBackground())){
            n.getBateauxJ1().get(0).resetBateau();
        }
        else if (l.getBackground().equals(contreTorpilleur1.getBackground())){
            n.getBateauxJ1().get(1).resetBateau();
        }
        else if (l.getBackground().equals(contreTorpilleur2.getBackground())){
            n.getBateauxJ1().get(2).resetBateau();
        }
        else if (l.getBackground().equals(croiseur.getBackground())){
            n.getBateauxJ1().get(3).resetBateau();
        }
        else if (l.getBackground().equals(porte_avion.getBackground())){
            n.getBateauxJ1().get(4).resetBateau();
        }
        this.supression = false;
        this.currentBoat = null;
        remplir();
    }

    /**
     * Place les bateaux sur le gridpane
     * @param mouseEvent Clique de souris
     */
    public void setBoat(MouseEvent mouseEvent) {
        if (this.supression) {
            deleteBoat(mouseEvent);
        } else {
            if (currentBoat != null) {
                Label l = (Label) mouseEvent.getSource();
                int x = GridPane.getRowIndex(l) == null ? 0 : GridPane.getRowIndex(l);
                int y = GridPane.getColumnIndex(l) == null ? 0 : GridPane.getColumnIndex(l);
                if (currentBoat == torpilleur) {
                    n.getBateauxJ1().get(0).placerBateau(x, y, n.getGrilleJ1());
                } else if (currentBoat == contreTorpilleur1) {
                    n.getBateauxJ1().get(1).placerBateau(x, y, n.getGrilleJ1());
                } else if (currentBoat == contreTorpilleur2) {
                    n.getBateauxJ1().get(2).placerBateau(x, y, n.getGrilleJ1());
                } else if (currentBoat == croiseur) {
                    n.getBateauxJ1().get(3).placerBateau(x, y, n.getGrilleJ1());
                } else if (currentBoat == porte_avion) {
                    n.getBateauxJ1().get(4).placerBateau(x, y, n.getGrilleJ1());
                }
                remplir();
            }
        }
    }

    /**
     * Sélectionne un des 5 bateaux
     * @param mouseEvent Clique de souris
     */
    public void selectionnedBoat(MouseEvent mouseEvent) {
        this.currentBoat = (GridPane) mouseEvent.getSource();
    }

    /**
     * Met à l'horizontale le bateau cliqué
     */
    public void setHorizontale() {
        if (currentBoat != null) {
            if (currentBoat == torpilleur) {
                n.getBateauxJ1().get(0).setDirection(Direction.Colonne);
            }
            else if (currentBoat == contreTorpilleur1) {
                n.getBateauxJ1().get(1).setDirection(Direction.Colonne);
            }
            else if (currentBoat == contreTorpilleur2) {
                n.getBateauxJ1().get(2).setDirection(Direction.Colonne);
            }
            else if (currentBoat == croiseur) {
                n.getBateauxJ1().get(3).setDirection(Direction.Colonne);
            }
            else if (currentBoat == porte_avion) {
                n.getBateauxJ1().get(4).setDirection(Direction.Colonne);
            }
        }
    }

    /**
     * Met à la verticale le bateau cliqué
     */
    public void setVerticale(){
        if (currentBoat != null) {
            if (currentBoat == torpilleur) {
                n.getBateauxJ1().get(0).setDirection(Direction.Ligne);
            }
            else if (currentBoat == contreTorpilleur1) {
                n.getBateauxJ1().get(1).setDirection(Direction.Ligne);
            }
            else if (currentBoat == contreTorpilleur2) {
                n.getBateauxJ1().get(2).setDirection(Direction.Ligne);
            }
            else if (currentBoat == croiseur) {
                n.getBateauxJ1().get(3).setDirection(Direction.Ligne);
            }
            else if (currentBoat == porte_avion) {
                n.getBateauxJ1().get(4).setDirection(Direction.Ligne);
            }
        }
    }

    /**
     * Valide la saisie des bateaux
     */
    public void valider() {
        n.remplirGrilleJ2();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Jeux/Bataille_navale_jeu.fxml"));
            fxmlLoader.setControllerFactory(c -> new NavaleJeuController(this.n));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Bataille navale");
            stage.setScene(scene);
            stage.show();
            Stage s1 = (Stage) boutonValider.getScene().getWindow();
            s1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
