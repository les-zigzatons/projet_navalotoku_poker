package affichage.jeux;

import affichage.menu.Menu;
import common.Joueur;
import common.Leaderboard;
import jeux.loto.Tirage;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;

import java.io.IOException;
import java.util.ArrayList;

public class LotoTirageController {

    /**
     * Numéro statique pour idéntifier les grilles de loto
     */
    private static Integer num;

    /**
     * Permet d'effectuer un tirage de numéro
     */
    private Tirage tirage;

    /**
     * Affichage du numéro tiré
     */
    public Label numero;

    /**
     * Affichage du temps restant
     */
    public Label time;

    /**
     * Temps restant
     */
    private int temps;

    /**
     * Permet de faire une animation sur le temps
     */
    private Timeline clock;

    /**
     * Initialise le timer et récupère la sauvegarde si elle existe
     */
    @FXML
    private void initialize() {
        
        if (Menu.app.getJoueur().getTirage() != null) {
            tirage = Menu.app.getJoueur().getTirage();
        }
        else {
            tirage = new Tirage();
        }
        numeroSuivant();
        temps = 10;
        clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            temps--;
            if(temps == 0){
                temps = 10;
                numeroSuivant();
            }
            time.setText(String.format("%02d", temps));
        }),
                new KeyFrame(Duration.seconds(1))
        );
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    /**
     * Sauvegarde la partie et retourne au menu
     * @param mouseEvent événement du clique de la souris
     */
    public void retourMenu(MouseEvent mouseEvent) {
        ArrayList<Stage> stagelist = new ArrayList<Stage>();
        Joueur j = Menu.app.getJoueur();
        clock.stop();

        j.setTirage(tirage);
        for(Window win : Window.getWindows()){
            if(win.getClass() == Stage.class)
                stagelist.add((Stage)win);
        }
        for(int i = 0; i < stagelist.size(); i++){
            stagelist.get(i).close();
        }
        if(mouseEvent == null) {
            int score = 0;
            for(int i = 0; i < Menu.app.getJoueur().getLoto().size(); i++){
                score += Menu.app.getJoueur().getLoto().get(i).score();
            }
            Leaderboard l = Menu.app.getJoueur().getLeaderboard();
            l.ajouterScoreLoto(Menu.app.getJoueur().getLoto().get(0).getNom(), score/ Menu.app.getJoueur().getLoto().size());
            Menu.app.getJoueur().setLeaderboard(l);
            Menu.app.getJoueur().setTirage(null);
            Menu.app.getJoueur().getLoto().clear();
        }
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Menu/Principal.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Selection du jeu ?");
            stage.setScene(scene);
            stage.show();

        }
        catch (IOException e){
            e.printStackTrace();
        }

    }

    /**
     * Tire le numéro suivant
     */
    private void numeroSuivant() {
        num = tirage.tirage();
        if(num == 0){

            retourMenu(null);
        }
        numero.setText(num.toString());
    }

    /**
     * Récupére le numéro tiré
     * @return Numéro du tirage
     */
    public static Integer getNumero(){
        return num;
    }
}
