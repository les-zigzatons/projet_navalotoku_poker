package jeux.sudoku;

import common.Case;
import common.Grille;

import java.io.Serializable;
import java.util.*;

public class Sudoku implements Serializable {

    private Grille grille;
    private Integer nbCoups;
    private int temps;
    private String nom;

    /**
     * Création du Sudoku avec un nombre de trou prédéfini
     * @param nom Nom du joueur
     */
    public Sudoku(String nom){
        nouvelleGrilleVide(9, 9);
        while(!remplirGrille());
        enleverNumero(30);
        nbCoups = 0;
        temps = 0;
        this.nom = nom;
    }

    /**
     * Création du Sudoku avec un nombre de trou demandé
     * @param nbtrous Nombre de trous
     * @param nom Nom du joueur
     */
    public Sudoku(int nbtrous, String nom){
        nouvelleGrilleVide(9, 9);
        while(!remplirGrille());
        enleverNumero(nbtrous);
        nbCoups = 0;
        temps = 0;
        this.nom = nom;
    }

    /**
     * Créer une nouvelle grille remplie de 0
     * @param x Coordonnées x
     * @param y Coordonnées y
     */
    private void nouvelleGrilleVide(int x, int y){
        Grille g = new Grille(x, y);
        g.remplirGrille(0);
        this.setGrille(g);
    }

    /**
     * Fonction récursive se basant sur l'algorithme de backtracking pour remplir la grille de Sudoku
     * @return Renvoie un boolean qui vérifie que la grille à été créer
     */
    public boolean remplirGrille(){
        boolean trouve = false;
        int x , y = 0, val;
        Stack<Integer> nombres = new Stack<Integer>();
        for(int n = 1; n <= grille.tailleX() ; n++){    // On met tout les tout les numéros possible dans une pile
            nombres.push(n);
        }
        Collections.shuffle(nombres);   // On mélange les éléments de la pile
        for(x = 0 ; !trouve && x < grille.tailleX() ; x++){     // On cherche une case vide
            for(y = 0; !trouve && y < grille.tailleY(); y++){
                if(((Integer)this.grille.getCase(x, y).getVal()) == 0){
                    trouve = true;
                }
            }
        }
        x--; y--;
        if(!trouve){    // Si il n'y a plus de case vide, le Sudoku est rempli
            return true;
        }

        for(int i = 0; i < grille.tailleX(); i++){
            val = nombres.pop();    // On récupère le premier élément de la pile
            if(verifNumero(x, y, val)){     // On vérifie que le numéro peux être sur la case
                grille.setCase(new Case<Integer>(val), x, y);   // On ajout le numéro à la case
                if(remplirGrille())     // On fait un appel récursif pour faire les prochains numéro
                    return true;

                grille.setCase(new Case<Integer>(0), x, y);     // Si la grille n'a pas pu être remplie, on remet la case à 0 et on retente avec un autre numéro
            }
        }
        return false;    // Si aucun numéro n'est possible, on retourne false
    }

    /**
     * Vérifie que la grille n'a qu'une seule solution
     * @return Renvoie un boolean qui dit s'il la grille n'a qu'une seule solution
     */
    private boolean estUnique(){
        int nbval;
        for(int x = 0; x < grille.tailleX(); x++){
            for(int y = 0; y < grille.tailleY(); y++){
                if((Integer)grille.getCase(x, y).getVal() == 0){        // On parcours toutes les cas qui ont un zéro
                    nbval = 0;
                    for(int i = 1; i <= grille.tailleX(); i++){     // On essaye les 9 chiffres, si un chiffre est valide, on incrémente nbval
                        if(verifNumero(x, y, i)){
                            nbval++;
                        }
                    }
                    if(nbval > 1){      // Si nbval est supérieur à 1, la solution n'est pas unique donc on retourne false
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Enlève un nombre k de numéro de la grille. Si le nombre d'élément est trop élevé, la dernière grille unique sera gardé
     * @param k Nombre de numéro à enlever
     */
    private void enleverNumero(int k){
        Random r = new Random();
        int x, y;
        int i = 0, trymax = 0;
        Case temp;
        while(i < k){
            x = r.nextInt(grille.tailleX());
            y = r.nextInt(grille.tailleY());     // On choisi une case au hasard
            if(((Case<Integer>)grille.getCase(x, y)).getVal() != 0) {
                temp = grille.getCase(x, y);
                grille.setCase(new Case<Integer>(0), x, y);     // On enlève le numéro tout en le gardant
                if(estUnique()){    // Si la solution est unique, on continue
                    i++;
                    trymax = 0;
                }
                else{       // Sinon on retente
                    trymax++;
                    grille.setCase(temp, x, y);
                }
                if(trymax > 1000){      // Si au bout de 1000 essaie on n'arrive pas à enlever un nouveau numéro, on renvoie le Sudoku
                    break;
                }
            }
        }
    }

    /**
     * Appel la fonction verifCol, verifLigne et verifCarre pour vérifier que l'ajout d'un nouvel entier est possible
     * @param x Coordonnée x
     * @param y Coordonné y
     * @param val Valeur à vérifier
     * @return Retrourne vrai si on peut ajouter le numéro sur la case, faux sinon
     */
    public boolean verifNumero(int x, int y, Integer val){
        return verifCol(x, val) && verifLigne(y, val) && verifCarre(x,y, val);
    }

    /**
     * Vérifie que l'on peut poser un entier sur une colonne
     * @param x Coordonnée x
     * @param val Valeur à vérifier
     * @return Retrourne vrai si on peut ajouter le numéro sur la colonne, faux sinon
     */
    private boolean verifCol(int x, Integer val){
        Case[] col = getGrille().getColonne(x);
        for (Case<Integer> c : col) {
            if(val.equals(c.getVal())){     // Vérifie que le paramètre val n'est pas sur la colonne
                return false;
            }
        }
        return true;
    }

    /**
     * Vérifie que l'on peut poser un entier sur une ligne
     * @param y Coordonnée y
     * @param val Valeur à vérifier
     * @return Retrourne vrai si on peut ajouter le numéro sur la ligne, faux sinon
     */
    private boolean verifLigne(int y, Integer val){
        Case[] ligne = getGrille().getLigne(y);
        for (Case<Integer> c: ligne) {
            if(val.equals(c.getVal())){     // Vérifie que le paramètre val n'est pas sur la ligne
                return false;
            }
        }
        return true;
    }

    /**
     * Vérifie que l'on peut poser un entier sur un carré
     * @param x Coordonnée x
     * @param y Coordonnée y
     * @param val Valeur à vérifier
     * @return Retrourne vrai si on peut ajouter le numéro sur un carré, faux sinon
     */
    private boolean verifCarre(int x , int y, Integer val){
        int sqrt = (int)Math.sqrt(grille.tailleX());    // Récupère la racine carré de la taille de la grille pour avoir la taille carré
        int carreX = x / sqrt;
        int carreY = y / sqrt;
        Case c = null;
        for(int i = 0; i < sqrt; i++){
            for(int j = 0; j < sqrt ; j++){
                c = this.getGrille().getCase(carreX*sqrt + i, carreY*sqrt +j);
                if(val.equals(c.getVal())){     //  Vérifie que le paramètre val n'est pas déjà dans le carré
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Insére une valeur à la position donnée
     * @param val Valeur à insérer
     * @param x Coordonnée x
     * @param y Coordonnée y
     */
    public void insererNumero(int val, int x, int y){
        this.grille.modifVal(val, x, y);
    }

    /**
     * Compte le nombre de numéro présent sur la grille
     * @param val Valeur à vérifier
     * @return Nombre de valeur sur la grille
     */
    public int countNumber(int val){
        int nb = 0;
        for(int i = 0; i < this.grille.tailleX(); i++){
             for(Case<Integer> element : this.grille.getColonne(i)){
                 if(Math.abs(element.getVal()) == val){
                     nb++;
                 }
             }
        }
        return nb;
    }

    public Grille getGrille() {
        return grille;
    }

    public void setGrille(Grille grille) {
        this.grille = grille;
    }

    public Integer getNbCoups() {
        return nbCoups;
    }

    public void setNbCoups(Integer nbCoups) {
        this.nbCoups = nbCoups;
    }

    public int getTemps() {
        return temps;
    }

    public void setTemps(int temps) {
        this.temps = temps;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
