package jeux.loto;

import java.util.Collections;
import java.util.Stack;

public class Tirage {

    /**
     * Pile de tout les numéros à tirer
     */
    private Stack<Integer> numeros;

    /**
     * Initialise le tirage
     */
    public Tirage(){
        numeros = new Stack<>();
        for(int i = 1; i <= 90; i++){
            getNumeros().add(i);
        }
        Collections.shuffle(getNumeros());
    }

    /**
     * Tire un numéro de la pile
     * @return Le numéro tirer
     */
    public Integer tirage(){
        if(getNumeros().isEmpty()){
            return 0;
        }
        return getNumeros().pop();
    }

    public Stack<Integer> getNumeros() {
        return numeros;
    }
}
