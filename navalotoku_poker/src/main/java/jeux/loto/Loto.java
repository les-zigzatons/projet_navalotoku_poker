package jeux.loto;

import common.Case;
import common.Grille;

import java.io.Serializable;
import java.util.*;


public class Loto implements Serializable {


    private Grille grille;
    private Stack<Integer> numeros;
    private String nom;

    // Constructeur
    public Loto(String nom){
        nouvelleGrille();
        this.nom = nom;
    }

    /**
     * methode pour creer une nouvelle grille vide de loto
     */
    private void nouvelleGrilleVide(){
        Grille g = new Grille(9,3);
        g.remplirGrille(0);
        this.grille = g;
    }

    public int score(){
        int v, k = 0;
        for(int i = 0; i < 3; i++){
            v = 0;
            for(int j = 0; j < 9; j++){
                if((Integer)grille.getCase(i,j).getVal() < 0){
                    v++;
                }
            }
            if(v == 5)
                k++;
        }
        if(k == 0)
            return 0;
        else if(k == 1)
            return 25;
        else if(k == 2)
            return 50;
        else
            return 100;
    }

    /**
     * Vérifie si la ligne est complète
     * @param i Index de la ligne
     * @return Nombre de case remplie dans la ligne
     */
    private int verifLigne(int i){
        int cpt = 0;
        for (int j = 0 ; j <=8 ; j++) {
            Case<Integer>c = (Case<Integer>)grille.getCase(j,i);
            if (c.getVal() < 0) {
                cpt++;
            }
        }
        return cpt;
    }


    /**
     * methode permettant de verifier sur le numero est tire et l'on pase la valeur a : -(val)
     * @param x ligne
     * @param y colonne
     */
    public void numeroTire(int x, int y){                      // si numero tire, on passe la valeur a - la valeur pour dire qu'elle est tire
        Case<Integer> c = (Case<Integer>)grille.getCase(x,y);
        this.grille.modifVal(-(c.getVal()), x, y);
    }

    /**
     * permet de creer une nouvelle grille de loto remplit pret a jouer
     */
    private void nouvelleGrille(){
        Random r = new Random();
        nouvelleGrilleVide();
        for (int x = 0 ; x < 9 ; x++){
            for (int y = 0 ; y < 3 ; y++){
                this.remplirCase(x, y);
            }
        }
        while(this.nbValLigne(0) > 5){
            supprimerCase(r.nextInt(9),0);
        }
        while(this.nbValLigne(1) > 5){
            supprimerCase(r.nextInt(9),1);
        }
        for(int i = 0; i < 9 && nbValLigne(2) > 5 ; i++){
            if(nbValColonne(i) == 3){
                supprimerCase(i, 2);
            }
        }
        for(int i = 0; i < 9 && nbValLigne(2) > 5 ; i++){
            if(nbValColonne(i) == 2){
                supprimerCase(i, 2);
            }
        }
    }

    /**
     * methode permettant de remplir la case passe en parametre
     * @param x la ligne
     * @param y la colonne
     */
    private void remplirCase(int x, int y){   // permet de remplir la case
        Random r = new Random();
        int nombreAleatoire = 0;
        do {
            nombreAleatoire = r.nextInt(10) + 1 + 10 * x;
        }while(this.grille.colonneContains(x, nombreAleatoire));
        this.grille.modifVal(nombreAleatoire, x, y);
    }

    /**
     * methode permettant de supprimer une case passe en parametre
     * @param x ligne
     * @param y colonne
     */
    private void supprimerCase( int x,int y){  // permet de passer la valeur d'une case à 0 ( supprimer la case )
        grille.modifVal(0, x, y);
    }

    /**
     * fonction permettant de compter le nombre valeur sur la colonne
     * @param x colonne
     * @return le nombre de valeur sur la colonne
     */
    private int nbValColonne(int x){
        int cpt = 0 ;
        for (int j  = 0 ; j < 3; j++){
            if(((Case<Integer>)grille.getCase(x,j)).getVal() != 0 ) cpt ++;
        }
        return cpt;
    }

    /**
     * fonction permettant de compter le nombre de valeur sur la ligne
     * @param y ligne
     * @return le nombre de valeur sur la ligne
     */
    private int nbValLigne(int y){      //renvoie le nombre de valeur sur la colonne
        int cpt = 0 ;
        for (int i  = 0 ; i < 9; i++){
            if(((Case<Integer>)grille.getCase(i,y)).getVal() != 0 ) cpt ++;
        }
        return cpt;
    }

    /**
     * @return la grille
     */
    public Grille getGrille(){
        return this.grille;
    }

    /**
     * permet de verifier si notre carton est complet, c'est a dire que tous les numeros sont tires
     * @return vrai si le carton est plein, sinon faux
     */
    public boolean verifVictoire(){
        for(int i = 0; i < 3; i++){
            if(verifLigne(i) != 5){
                return false;
            }
        }
        return true;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
