package jeux.poker;

import java.io.Serializable;
import java.util.Collections;
import java.util.Stack;

public class Paquet implements Serializable {

    Stack<Carte> paquet;

    /**
     * constructeur sans parametres
     */
    public Paquet(){
        nouveauPaquet();
    }

    /**
     * creation d'un nouveau paquet
     */
    public void nouveauPaquet(){
        remplirPaquet();
        melangePaquet();
    }

    /**
     * permet de remplir le nouveau paquet de carte
     */
    private void remplirPaquet(){
        this.paquet = new Stack<Carte>();
        for(int i = 0; i < Couleur.values().length; i++){
            for(int j = 2; j <= 14; j++){
                this.paquet.push(new Carte(j,Couleur.intToCouleur(i)));
            }
        }
    }

    /**
     * permet de tirer une carte du paquet
     * @return une carte du paquet
     */
    public Carte tirerCarte(){
        return this.paquet.pop();
    }

    /**
     * permet de melanger le paquet
     */
    private void melangePaquet(){
        Collections.shuffle(this.paquet);
    }

}
