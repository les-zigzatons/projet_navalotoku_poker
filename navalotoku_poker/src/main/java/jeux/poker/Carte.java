package jeux.poker;

import java.io.Serializable;

public class Carte implements Comparable<Carte>, Serializable {

    private int rang;
    private Couleur couleur;

    /**
     * constructeur avec parametre
     * @param rang Rang de la carte
     * @param couleur Couleur de la carte
     */
    public Carte(int rang, Couleur couleur){
        this.couleur = couleur;
        this.rang = rang;
    }

    /**
     * Permet de compare deux carte est renvoie 1 si la carte que l'on a est superieur a celle passe en parametre
     * @param c
     * @return
     */
    public int compareTo(Carte c) {
        if(this.getRang() == c.getRang()){
            return 0;
        }
        else if(this.getRang() > c.getRang()){
            return 1;
        }
        else{
            return -1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        Carte c;
        if(obj.getClass() == Carte.class){
            c = (Carte)obj;
            return c.getRang() == this.getRang() && c.getCouleur() == this.getCouleur();
        }
        return false;
    }


    /**
     * permet de renvoyer le nom de la figure + la couleur
     */
    @Override
    public String toString() {
        String res;
        switch (this.getRang()){
            case 14:
                res = "A";
                break;
            case 11:
                res = "J";
                break;
            case 12:
                res = "Q";
                break;
            case 13:
                res = "K";
                break;
            default:
                res = String.valueOf(this.getRang());
        }
        return res + getCouleur().toString();
    }

    /**
     * permet de retourner le rang
     * @return
     */
    public int getRang() {
        return rang;
    }

    /**
     * permet de renvoyer l
     * @return
     */
    public Couleur getCouleur() {
        return couleur;
    }
}
