package jeux.poker;

import java.io.Serializable;
import common.Pair;

import java.util.Arrays;

public class Mains implements Comparable<Mains>, Serializable {

    private Carte[] cartes;
    private int mise;
    private int jetons;
    private boolean coucher;
    private String nom;

    public Mains(String nom){
        cartes = new Carte[5];
        coucher = false;
        mise = 0;
        jetons = 300;
        this.nom = nom;
    }

    /**
     * r
     * @return la main
     */
    public Carte[] getMain(){
        return this.cartes;
    }

    /**
     * permet de tirer 5 cartes parmi le paquet
     * @param p correspond au paquet
     */
    public void tirerCartes(Paquet p){
        for(int i = 0; i < 5; i++){
            cartes[i] = p.tirerCarte();
        }
        rangerMain();
    }

    /**
     * permet d'echanger une carte la main avec un carte tire dans le paquet
     * @param p correspon au paquet
     * @param position correspond a la position de la carte que l'on veut changer
     */
    public void echangerCarte(Paquet p, int position){
        this.cartes[position] = p.tirerCarte();
    }

    /**
     * permet de ranger les cartes de notre main selon la hauteur
     */
    public void rangerMain(){
        Arrays.sort(cartes);
    }

    public boolean isCoucher() {
        return coucher;
    }

    public void seCoucher(){
        coucher = true;
    }

    public void seReveiller(){ coucher = false; }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for(Carte c : this.cartes){
            res.append(c.toString()).append("\n");
        }
        return res.toString();
    }

    /**
     * fonction qui compare la main des differents joueurs
     * @param m correspond au main du joureur
     * @return le gagnant
     */
    public int compareTo(Mains m){
        Pair<Evaluation, Carte> j1 = this.evaluation();
        Pair<Evaluation, Carte> j2 = m.evaluation();
        if(j1.getT().getValeur() > j2.getT().getValeur()){
            return 1;
        }
        else if(j1.getT().getValeur() < j2.getT().getValeur()){
            return -1;
        }
        if(j1.getV().getRang() > j2.getV().getRang()){
            return 1;
        }
        else if(j1.getV().getRang() < j2.getV().getRang()){
            return -1;
        }
        if(j1.getT() == Evaluation.Full){
            return this.getHauteurPaireDansFull().compareTo(m.getHauteurPaireDansFull());
        }
        else if(j1.getT() ==  Evaluation.Paire || j1.getT() == Evaluation.Brelan){
            int i = 1;
            Carte c = this.cartes[0];
            for (Carte c1: this.cartes) {
                if(c.getRang() < c1.getRang()){
                    c = c1;
                }
            }
            for(Carte c2 : m.cartes){
                if(c.getRang() < c2.getRang()){
                    i = 2;
                    c = c2;
                }
            }
            return i;
        }
        if(this.carteHaute().getRang() > m.carteHaute().getRang()){
            return  1;
        }
        else if(this.carteHaute().getRang() > m.carteHaute().getRang()){
            return -1;
        }
        return 0;
    }

    /**
     * Permet de savoir quelle combinaison le joueur obtient
     * @return Paire avec la carte et son évaluation
     */
    public Pair<Evaluation, Carte> evaluation(){
        Carte c;
        if((c = verifQuiteRoyal()) != null) return new Pair<>(Evaluation.QuinteRoyal, c);
        else if((c = verifQuiteFlush()) != null) return new Pair<>(Evaluation.QuinteFlush, c);
        else if((c = verifCarre()) != null) return new Pair<>(Evaluation.Carre, c);
        else if((c = verifFull()) != null) return new Pair<>(Evaluation.Full, c);
        else if((c = verifCouleur()) != null) return new Pair<>(Evaluation.Couleur, c);
        else if((c = verifSuite()) != null) return new Pair<>(Evaluation.Suite, c);
        else if((c = verifBrelan()) != null) return new Pair<>(Evaluation.Brelan, c);
        else if((c = verifDeuxPaire()) != null) return new Pair<>(Evaluation.DeuxPaire, c);
        else if((c = verifPaire()) != null) return new Pair<>(Evaluation.Paire, c);
        else return new Pair<>(Evaluation.Rien, carteHaute());
    }

    /**
     * permet de verifier si l'on a une quinte flush royal
     * @return la carte haute de notre quinte flush royal
     */
    private Carte verifQuiteRoyal(){
        int val = 0;
        if(cartes[0].getRang() == 9 && cartes[1].getRang() == 10 && cartes[2].getRang() == 11 && cartes[3].getRang() == 12 && cartes[4].getRang() == 13){
            for(Carte c : cartes){
                if(c.getCouleur() == cartes[0].getCouleur()){
                    val++;
                }
            }
        }
        if(val == 5){
            return carteHaute();
        }
        return null;
    }

    /**
     * verifie si le joueur a une quinte flush
     * @return renvoie la haute de notre quinte flush
     */
    private Carte verifQuiteFlush(){
        Carte tmp = cartes[0];
        for (int i = 1 ; i < cartes.length; i++) {
            if(tmp.getRang() + 1 == cartes[i].getRang() && tmp.getCouleur() == cartes[i].getCouleur()){
                tmp = cartes[i];
            }
            else return null;
        }
        return carteHaute();
    }

    /**
     * verifie si l'on a un carre
     * @return la hauteur de notre carre
     */
    private Carte verifCarre(){
        if(cartes[0].getRang() == cartes[3].getRang()){
            return cartes[0];
        }
        else if(cartes[1].getRang() == cartes[4].getRang()){
            return cartes[1];
        }
        return null;
    }

    /**
     * verifie si l'on a un full
     * @return le resultat de notre verification brelan
     */
    private Carte verifFull(){
        if((cartes[0].getRang() == cartes[2].getRang() && cartes[3].getRang() == cartes[4].getRang()) || (cartes[0].getRang() == cartes[1].getRang() && cartes[2].getRang() == cartes[4].getRang())){
            return verifBrelan();
        }
        return null;
    }

    /**
     * permet de verifier si l'on a une couleur
     * @return l'hauteur de notre couleur
     */
    private Carte verifCouleur(){
        Carte tmp = cartes[0];
        int nb = 1;
        for(int i = 1; i < cartes.length; i++){
            if(cartes[i].getCouleur() == tmp.getCouleur()){
                nb++;
            }
        }
        if(nb == 5){
            return carteHaute();
        }
        return null;
    }

    /**
     * permet de voir si le joueur a une suite
     * @return la hauteur de notre suite
     */
    private Carte verifSuite(){
        Carte tmp = cartes[0];
        for (int i = 1 ; i < cartes.length; i++) {
            if(tmp.getRang() + 1 == cartes[i].getRang()){
                tmp = cartes[i];
            }
            else return null;
        }
        return carteHaute();
    }

    /**
     * permet de voir si le joueur a un brelan
     * @return la valeur du brelan
     */
    private Carte verifBrelan(){
        if(cartes[0].getRang() == cartes[2].getRang()){
            return cartes[0];
        }
        else if(cartes[1].getRang() == cartes[3].getRang()){
            return cartes[1];
        }
        else if(cartes[2].getRang() == cartes[4].getRang()){
            return cartes[2];
        }
        return null;
    }

    /**
     * permet de verifier si l'on a deux paire
     * @return la valeur des deux paires
     */
    private Carte verifDeuxPaire(){
        int nb = 0;
        Carte c = null;
        for(int i = 0; i < cartes.length -1 ; i++){
            if(cartes[i].getRang() ==  cartes[i +1].getRang()){
                if(c == null || c.getRang() < cartes[i].getRang()){
                    c = cartes[i];
                }
                nb++;
            }
        }
        if(nb == 2) return c;
        return null;

    }

    /**
     * permet de verifier si l'on a une paire
     * @return retourne notre paire
     */
    private Carte verifPaire(){
        for(int i = 0; i < cartes.length -1 ; i++){
            if(cartes[i].getRang() == cartes[i +1].getRang())
                return cartes[i];
        }
        return null;
    }

    /**
     * permet de verifier la carte la plus haute
     * @return la carte la plus haute
     */
    private Carte carteHaute(){
        Carte tmp = cartes[0];
        for(int i = 1; i < cartes.length; i++){
            if(cartes[i].getRang() > tmp.getRang()) tmp = cartes[i];
        }
        return tmp;
    }

    /**
     * permet de verifier entre 2 full lequel gagne : si l'on a un brelan identique des 2 cotes du fait nous allons verifier la paire la plus forte entre les 2 mains
     * @return la paire la plus grande entre  2 full
     */
    private Carte getHauteurPaireDansFull(){
        Pair<Integer, Carte> p1 = null;
        Pair<Integer, Carte> p2 = null;
        int i = 0;
        for(Carte carte : this.getMain()){
            if(p1 == null){
                p1 = new Pair<Integer, Carte>(1, carte);
            }
            else if(p2 == null){
                p2 = new Pair<Integer, Carte>(1, carte);
            }
            else{
                if(p1.getV().compareTo(carte) == 0){
                    p1.setT(p1.getT() + 1);
                }
                else if(p2.getV().compareTo(carte) == 0){
                    p2.setT(p2.getT() + 1);
                }
            }
        }
        if(p1.getT() > p2.getT())
            return p2.getV();
        return p2.getV();
    }

    public int getMise(){
        return mise;
    }

    public void setMise(int mise){
        this.mise += Math.min(mise, jetons);
    }

    public void resetMise(){
        this.mise = 0;
    }

    public int getJetons(){
        return jetons;
    }

    public void addJetons(int jetons){
        this.jetons += jetons;
    }

    public void removeJetons(int jetons){
        if(jetons > this.jetons)
            this.jetons = 0;
        else
            this.jetons -= jetons;
    }

    public String getNom() {
        return nom;
    }
}
