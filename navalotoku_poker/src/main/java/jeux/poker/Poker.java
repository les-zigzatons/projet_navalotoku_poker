package jeux.poker;

import java.io.Serializable;
import java.util.*;

public class Poker implements Serializable {

    private List<Mains> joueurs;
    private Paquet paquet;
    private int misetotal;
    private boolean premiertour;

    /**
     * Créer une nouvelle partie
     * @param joueurs Liste des noms des joueurs
     */
    public Poker(List<String> joueurs){
        this.joueurs = new ArrayList<>();
        for(int i = 0; i < joueurs.size(); i++){
            this.joueurs.add(new Mains(joueurs.get(i)));
        }
        this.paquet = new Paquet();

    }

    /**
     * fonction qui attribut les cartes aux joueurs
     */
    public void premierTour(){
        for(int i = 0; i < joueurs.size(); i++){
            joueurs.get(i).tirerCartes(paquet);
        }
        premiertour = true;
    }

    /**
     * fonction qui permet au joueur d'echanger une ou plusieurs cartes
     */
    public void secondTour(){
        this.premiertour = !this.premiertour;
    }

    /**
     *
     * @param m main du joueur
     * @return la main du joueur trie
     */
    private List<Integer> echangeCarteJoueur(Mains m){
        System.out.println("Quelles cartes voulez-vous echanger ? (0 pour valider)");
        List<Integer> list = new ArrayList<Integer>();
        int v;
        do {
            Scanner c = new Scanner(System.in);
            try{
                v = c.nextInt();
                if(v <= 5 && v > 0){
                    list.add(v);
                }
                else if(v != 0){
                    System.out.println("Carte invalide");
                }
            }catch(InputMismatchException e){
                v = Integer.MAX_VALUE;
                System.out.println("Carte invalide");
            }
        }while (v != 0);
        m.rangerMain();
        return list;
    }

    /**
     *
     * @param i correspond au numéro du joueurs
     * @return la main du joueur i
     */
    public Mains getMain(int i){
        return this.joueurs.get(i);
    }

    public int nbJoueurs(){
        return joueurs.size();
    }

    public Paquet getPaquet() {
        return paquet;
    }

    public int getMisetotal() {
        return misetotal;
    }

    public void setMisetotal(int mise) {
        this.misetotal = mise;
    }

    public void addMisetotal(int mise){
        this.misetotal += mise;
    }

    public boolean isPremiertour() {
        return premiertour;
    }

    public void finJeu(){
        Mains m = Collections.max(joueurs);
        m.addJetons(misetotal);
        misetotal = 0;
        for(Mains j : joueurs){
            j.resetMise();
            j.seReveiller();
        }
        joueurs.removeIf(e -> e.getJetons() == 0);
        paquet = new Paquet();
        premierTour();
    }

    public String getGagnant(){
        Mains m = Collections.max(joueurs);
        int i = 1;
        for(Mains j : joueurs){
            if(j == m){
                return "Le gagnant est le joueur " + j.getNom();
            }
            i++;
        }
        return "";
    }
}
