package jeux.poker;

public enum Couleur {
    Pique,
    Trefle,
    Carreau,
    Coeur;

    /**
     * constructeur avec parametre qui renvoie la couleur pique,trefle,carreau,ou coeur
     * @param val
     * @return
     */
    public static Couleur intToCouleur(int val){
        switch (val){
            case 0:
                return Pique;
            case 1:
                return Trefle;
            case 2:
                return Carreau;
            case 3:
                return Coeur;
            default:
                return null;
        }
    }

    public String toString(){
        switch (this){
            case Coeur:
                return "H";
            case Pique:
                return "S";
            case Trefle:
                return "C";
            case Carreau:
                return "D";
        }
        return null;
    }
}
