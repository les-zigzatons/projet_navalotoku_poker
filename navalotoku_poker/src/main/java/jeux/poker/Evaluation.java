package jeux.poker;

public enum Evaluation {
    Rien(0),
    Paire(1),
    DeuxPaire(2),
    Brelan(3),
    Suite(4),
    Couleur(5),
    Full(6),
    Carre(7),
    QuinteFlush(8),
    QuinteRoyal(9);

    private int valeur;

    Evaluation(int n){
        this.valeur = n;
    }

    public int getValeur(){ return this.valeur; }
}
