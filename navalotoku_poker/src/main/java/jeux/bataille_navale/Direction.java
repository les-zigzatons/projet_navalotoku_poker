package jeux.bataille_navale;

import java.util.Random;

public enum Direction {
    Ligne,
    Colonne;

    /**
     * Récupère une direction aléatoire
     * @return Direction
     */
    public static Direction getOne(){
        Random r = new Random();
        int val = r.nextInt(2);
        if(val == 0){
            return Ligne;
        }
        return Colonne;
    }
}

