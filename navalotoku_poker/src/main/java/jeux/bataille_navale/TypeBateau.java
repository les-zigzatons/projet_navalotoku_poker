package jeux.bataille_navale;

public enum TypeBateau {

    PorteAvion (5),
    Croiseur(4),
    ContreTorpilleur1(3),
    ContreTorpilleur2 (3),
    Torpilleur(2);

    private int number;

    /**
     * Constructeur de TypeBateau
     * @param n
     */
    TypeBateau(int n){
        this.number = n;
    }

    /**
     * Renvoie la taille du bateau
     * @return
     */
    public int getTaille(){
        return this.number;
    }
}

