package jeux.bataille_navale;

import common.Grille;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Navale implements Serializable {

    private Grille grilleJ1;
    private List<Bateau> bateauxJ1;

    private Grille grilleJ2;
    private List<Bateau> bateauxJ2;

    private String nom;

    /**
     * Constructeur de la classe Navale
     * @param nom Nom du joueur
     */
    public Navale(String nom) {
        nouvelleGrilleVide();
        creerBateau();
        this.nom = nom;
    }

    /**
     * Création des grilles grilleJ1 et grilleJ2 initialisées à vide
     */
    private void nouvelleGrilleVide(){
        Grille g1 = new Grille(10, 10);
        Grille g2 = new Grille(10, 10);
        g1.remplirGrille(TypeCellule.Vide);
        g2.remplirGrille(TypeCellule.Vide);
        this.grilleJ1 = g1;
        this.grilleJ2 = g2;

    }

    /**
     * Création des listes bateauxJ1 et bateauxJ2 initialisées grâce à nouvelleListeBateau
     */
    private void creerBateau(){
        bateauxJ1 = nouvelleListeBateau();
        bateauxJ2 = nouvelleListeBateau();
    }

    /**
     * Création d'une liste bateau contenant les 5 bateaux
     * @return la liste de bateau
     */
    private List<Bateau> nouvelleListeBateau(){
        List<Bateau> b = new ArrayList<Bateau>();
        b.add(new Bateau(TypeBateau.Torpilleur));
        b.add(new Bateau(TypeBateau.ContreTorpilleur1));
        b.add(new Bateau(TypeBateau.ContreTorpilleur2));
        b.add(new Bateau(TypeBateau.Croiseur));
        b.add(new Bateau(TypeBateau.PorteAvion));
        return b;
    }

    /**
     * Remplie de façon aléatoire la grille de bateaux
     */
    public void remplirGrilleJ2() {
        Random r = new Random();
        for (Bateau b: bateauxJ2) {
            b.setDirection(Direction.getOne());
            while(!b.placerBateau(r.nextInt(grilleJ2.tailleX()), r.nextInt(grilleJ2.tailleY()), grilleJ2));
        }
    }

    /**
     * Retourne la grilleJ1
     * @return
     */
    public Grille getGrilleJ1(){
        return grilleJ1;
    }

    /**
     * Retourne la grilleJ2
     * @return
     */
    public Grille getGrilleJ2() {
        return grilleJ2;
    }

    /**
     * Retourne la grille de bateauxJ1
     * @return
     */
    public List<Bateau> getBateauxJ1() {
        return bateauxJ1;
    }

    /**
     * Retourne la grille de bateauxJ2
     * @return
     */
    public List<Bateau> getBateauxJ2() {
        return bateauxJ2;
    }

    /**
     * Tire sur une case du Joueur 1
     * @param x Position x du tire
     * @param y Position y du tire
     */
    public void tireJ1(int x ,int y){
        if(grilleJ1.getCase(x, y).getVal() == TypeCellule.Bateau){
            grilleJ1.modifVal(TypeCellule.Touche, x, y);
            verifCouleJ1();
        }
        else {
            grilleJ1.modifVal(TypeCellule.Rate, x, y);
        }
    }

    /**
     * Tire sur une case du Joueur 2
     * @param x Position x du tire
     * @param y Position y du tire
     */
    public void tireJ2(int x ,int y){
        if(grilleJ2.getCase(x, y).getVal() == TypeCellule.Bateau){
            grilleJ2.modifVal(TypeCellule.Touche, x, y);
            verifCouleJ2();
        }
        else {
            grilleJ2.modifVal(TypeCellule.Rate, x, y);
        }
    }

    /**
     * Tire sur une case aléatoire du Joueur 1
     */
    public void tireJ1Rand(){
        Random r = new Random();
        int x, y;
        do{
            x = r.nextInt(10);
            y = r.nextInt(10);
        }while(grilleJ1.getCase(x, y).getVal() != TypeCellule.Vide && grilleJ1.getCase(x, y).getVal() != TypeCellule.Bateau);
        tireJ1(x,y);
    }

    /**
     * Vérifie si un des bateaux du joueur 1 est coulé après un des tirs du joueur 2
     */
    private void verifCouleJ1(){
        for (Bateau b: this.bateauxJ1) {
            if(b.isCoule()){
                b.setCoule();
            }
        }
    }

    /**
     * Vérifie si un des bateaux du joueur 1 est coulé après un des tirs du joueur 2
     */
    private void verifCouleJ2(){
        for (Bateau b: this.bateauxJ2) {
            if(b.isCoule()){
                b.setCoule();
            }
        }
    }

    /**
     * Vérifie si tout les bateaux du joueur 1 sont placer
     * @return Vrai s'il sont tous placer
     */
    public boolean tousPlacer(){
        for(Bateau b : bateauxJ1){
            if(!b.isPlaced())
                return false;
        }
        return true;
    }

    /**
     * Verifie si le joueur 1 à gagner
     * @return Vrai s'il à gagner
     */
    public boolean verifVictoireJ1(){
        boolean verif = true;
        for(Bateau b : bateauxJ2){
            if(!b.isCoule())
                verif = false;
        }
        return verif;
    }

    /**
     * Verifie si le joueur 2 à gagner
     * @return Vrai s'il à gagner
     */
    public boolean verifVictoireJ2(){
        boolean verif = true;
        for(Bateau b : bateauxJ1){
            if(!b.isCoule())
                verif = false;
        }
        return verif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
