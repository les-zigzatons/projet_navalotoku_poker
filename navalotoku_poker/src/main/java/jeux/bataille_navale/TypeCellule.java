package jeux.bataille_navale;

public enum TypeCellule {
    Vide,
    Bateau,
    Touche,
    Rate,
    Coule
}
