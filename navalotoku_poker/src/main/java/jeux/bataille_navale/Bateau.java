package jeux.bataille_navale;

import common.Case;
import common.Grille;

import java.io.Serializable;

public class Bateau implements Serializable {

    private Case[] element;
    private Direction direction;
    private boolean estplace;
    private TypeBateau typeBateau;

    /**
     * Constructeur de la classe Bateau
     * @param t Type du bateau
     */
    public Bateau(TypeBateau t) {
        this.direction = Direction.Ligne;
        this.typeBateau = t;
        this.setTailleBateau();
        this.estplace = false;

    }

    /**
     * Placement d'un bateau grâce à ses coordonnées et la grille
     * @param x Position x du bateau
     * @param y Position y du bateau
     * @param g Grille du bateau
     * @return Vrai s'il est placé
     */
    public boolean placerBateau(int x, int y, Grille g) {
        if ((verifPlacement(x, y, g)) && (!isPlaced())) {
            if (this.getDirection() == Direction.Ligne) {
                for (int i = 0; i < this.getTailleBateau(); i++) {
                    g.setCase(this.element[i], x + i, y);
                }
            } else {
                for (int i = 0; i < this.getTailleBateau(); i++) {
                    g.setCase(this.element[i], x, y + i);
                }
            }
            estplace = true;
            return true;
        }
        return false;
    }

    /**
     * Vérifie si un bateau peut être placé
     * @param x Position x du bateau
     * @param y Position y du bateau
     * @param g Grille du bateau
     * @return Vrai s'il est plaçable
     */
    private boolean verifPlacement(int x, int y, Grille g) {
        if (this.getDirection() == Direction.Ligne && x + getTailleBateau() <= g.tailleX()) {
            for (int i = 0; i < this.getTailleBateau(); i++) {
                if (g.getCase(x + i, y).getVal() == TypeCellule.Bateau) {
                    return false;
                }
            }
            return true;
        } else if (this.getDirection() == Direction.Colonne && y + getTailleBateau() <= g.tailleY()) {
            for (int i = 0; i < this.getTailleBateau(); i++) {
                if (g.getCase(x, y + i).getVal() == TypeCellule.Bateau) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Supprime un bateau
     */
    public void resetBateau() {
        for (int i = 0; i < element.length; i++) {
            element[i].setVal(TypeCellule.Vide);
        }
        setTailleBateau();
        this.estplace = false;
    }

    /**
     * Vérifie si un bateau est placé
     * @return Vrai s'il est placé
     */
    public boolean isPlaced() {
        return estplace;
    }

    /**
     * Définie la taille du bateau
     */
    private void setTailleBateau() {
        int val = this.typeBateau.getTaille();
        this.element = new Case[val];
        for (int i = 0; i < val; i++) {
            this.element[i] = new Case<>(TypeCellule.Bateau);
        }
    }

    /**
     * Retourne la taille d'un bateau
     * @return
     */
    public int getTailleBateau() {
        return this.typeBateau.getTaille();
    }

    /**
     * Retourne la direction d'un bateau
     * @return
     */
    public Direction getDirection() {
        return this.direction;
    }

    /**
     * Modifie la direction d'un bateau
     * @param d
     */
    public void setDirection(Direction d) {
        this.direction = d;
    }

    /**
     * Retourne vrai ou faux si un bateau est coulé
     * @return Vrai s'il est coulé
     */
    public boolean isCoule() {
        for (Case<TypeCellule> c : this.element) {
            if (c.getVal() == TypeCellule.Bateau) {
                return false;
            }
        }
        return true;
    }

    /**
     * Modifie si un bateau est coulé ou non
     */
    protected void setCoule() {
        for (int i = 0; i < element.length; i++) {
            this.element[i].setVal(TypeCellule.Coule);
        }
    }

    public TypeBateau getTypeBateau(){
        return this.typeBateau;
    }
}
