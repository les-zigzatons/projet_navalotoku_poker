module navalotoku.poker {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    opens affichage.menu to javafx.graphics,javafx.fxml;
    opens affichage.jeux to javafx.graphics,javafx.fxml;
    exports affichage.menu;
    exports affichage.jeux;
}