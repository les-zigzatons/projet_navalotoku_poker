package common;

import java.io.Serializable;

public class Grille implements Serializable {
    private Case[][] matrice;

    /**
     * Constructeur de la grille
     * @param x Taille horizontale
     * @param y Taille verticale
     */
    public Grille(int x, int y){
        this.matrice = new Case[x][y];
    }

    /**
     * Récupère la largeur
     * @return Largeur
     */
    public int tailleX(){
        return matrice.length;
    }

    /**
     * Récupère la hauteur
     * @return Hauteur
     */
    public int tailleY(){
        return matrice[0].length;
    }

    /**
     * Récupère la case à la postion donnée
     * @param x Position x
     * @param y Position y
     * @return Case
     */
    public Case<?> getCase(int x, int y){
        return this.matrice[x][y];
    }

    /**
     * Définie la case à position donnée
     * @param cas Case à définir
     * @param x Position x
     * @param y Position y
     */
    public void setCase(Case<?> cas, int x, int y){
        this.matrice[x][y] = cas;
    }

    /**
     * permet de remplir un grille avec des valeurs de n'importe quelle type
     * @param val Valeur de remplissage
     * @param <T> Type générique
     */
    public <T> void remplirGrille(T val){
        for(int x = 0 ; x < tailleX(); x++){
            for(int y = 0; y < tailleY(); y++){
                this.matrice[x][y] = new Case<T>(val);
            }
        }
    }

    /**
     * Récupère la colonne
     * @param x Position
     * @return la colonne d'une matrice
     */
    public Case[] getColonne(int x){
        return this.matrice[x];
    }

    /**
     * Récupère la ligne
     * @param y position de la ligne
     * @return les lignes d'une matrice
     */
    public Case[] getLigne(int y){
        Case[] ligne = new Case[this.tailleX()];
        for(int i = 0; i < this.tailleX(); i++){
            ligne[i] = matrice[i][y];
        }
        return ligne;
    }

    /**
     * permet de modifier une valeur de type T dans notre matrice
     * @param val valeur
     * @param x ligne
     * @param y colonne
     * @param <T> le type de la valeur
     */
    public <T> void modifVal(T val, int x, int y) {
        this.matrice[x][y].setVal(val);
    }

    /**
     * permet de verifier si sur la ligne definit la valeur est dans la colonne
     * @param x la ligne
     * @param val valeur
     * @param <T> le type de la valeur
     * @return vrai si elle appartient, sinon faux
     */
    public <T> boolean colonneContains(int x, T val){
        for(int i = 0; i < this.tailleY(); i++){
            if(this.matrice[x][i].getVal() == val){
                return true;
            }
        }
        return false;
    }

}

