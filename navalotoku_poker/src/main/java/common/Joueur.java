package common;

import jeux.loto.Loto;
import jeux.loto.Tirage;
import jeux.poker.Poker;
import jeux.sudoku.*;
import jeux.bataille_navale.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Joueur implements Serializable{
    private Sudoku s;
    private Navale n;
    private List<Loto> l;
    private Poker p;
    private Tirage t;
    private Leaderboard leaderboard;

    public Joueur() {
        l = new ArrayList<>();
        leaderboard = new Leaderboard();
    }

    /**
     * Methode permettant de sauvegarder la partie d'un joueur
     * @param outFile
     */
    public void sauvegarder(File outFile){
        try {
            FileOutputStream outFileStream = new FileOutputStream(outFile);
            ObjectOutputStream outObjectStream = new ObjectOutputStream(outFileStream);
            Joueur j = this;
            outObjectStream.writeObject(j);
            outObjectStream.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode permettant de charger la partie d'un joueur
     * @param inFile
     */
    public void charger(File inFile){
        FileInputStream inFileStream;
        try {
            inFileStream = new FileInputStream(inFile);
            ObjectInputStream inObjectStream = new ObjectInputStream(inFileStream);
            Joueur j;
            j = (Joueur) inObjectStream.readObject();
            this.setSudoku(j.getSudoku());
            this.setNavale(j.getNavale());
            this.setLoto(j.getLoto());
            this.setPoker(j.getPoker());
            this.setLeaderboard(j.getLeaderboard());
            inObjectStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Sudoku getSudoku() {
        return s;
    }

    /**
     * permet d'ajouter un sudoku au joueur
     * @param s le sudoku passe en parametre
     */
    public void setSudoku(Sudoku s) {
        this.s = s;
    }

    /**
     *
     * @return la bataille navaille auquel le joueur est associe
     */
    public Navale getNavale() {
        return n;
    }

    /**
     * permet d'ajouter une bataille navale au joueur
     * @param n la bataille navale passe en parametre
     */
    public void setNavale(Navale n) {
        this.n = n;
    }

    /**
     *
     * @return le loto auquel le joueur est associe
     */
    public List<Loto> getLoto() {
        return l;
    }

    /**
     * permet d'ajouter une partie loto au joueur
     * @param l le loto passe en parametre
     */
    public void setLoto(List<Loto> l) {
        this.l = l;
    }

    /**
     * permet d'ajouter une grille de loto a notre liste de grille loto du joueur
     * @param l la grille de loto à ajouter a la liste des grilles du joueur
     */
    public void addLoto(Loto l){ this.l.add(l); }

    /**
     *
     * @return la partie de poker p a laquelle le joueur est associe
     */
    public Poker getPoker() {
        return p;
    }

    /**
     * permet d'ajouter une partie de poker au joueur
     * @param p la partie de poker passe en parametre
     */
    public void setPoker(Poker p) {
        this.p = p;
    }

    public Tirage getTirage(){ return t; }

    public void setTirage(Tirage t){ this.t = t; }

    public Leaderboard getLeaderboard() {
        return leaderboard;
    }

    public void setLeaderboard(Leaderboard leaderboard) {
        this.leaderboard = leaderboard;
    }
}
