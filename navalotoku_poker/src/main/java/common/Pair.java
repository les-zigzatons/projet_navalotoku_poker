package common;

import java.io.Serializable;

public class Pair<T, V> implements Comparable<Pair<T,Integer>>, Serializable {
    private T t;
    private V v;

    /**
     * Paire d'objet
     * @param t Objet 1
     * @param v Objet 2
     */
    public Pair(T t, V v){
        this.setT(t);
        this.setV(v);
    }


    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public V getV() {
        return v;
    }

    public void setV(V v) {
        this.v = v;
    }

    @Override
    public int compareTo(Pair<T, Integer> tvPair) {
        if(this.v.getClass() == Integer.class){
            return tvPair.getV().compareTo((Integer)this.v);
        }
        return 0;
    }

    public String toString(){
        return t.toString() + " : " + v.toString();
    }
}
