package common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Leaderboard implements Serializable {
    private static final long serialVersionUID = 45616546545676456L;

    private List<Pair<String, Integer>> scoresLoto;
    private List<Pair<String, Integer>> scoresSudoku;
    private List<Pair<String, Integer>> scoresNavale;
    private List<Pair<String, Integer>> scoresPoker;

    /**
     * Initialise le leaderboard
     */
    public Leaderboard(){
        scoresSudoku = new ArrayList<>();
        scoresLoto = new ArrayList<>();
        scoresNavale = new ArrayList<>();
        scoresPoker = new ArrayList<>();
    }

    /**
     * Ajoute un score au Loto
     * @param pseudo Pseudo du joueur
     * @param score Score du joueur
     */
    public void ajouterScoreLoto(String pseudo, Integer score){
        scoresLoto.add(toPair(pseudo,score));
        Collections.sort(scoresLoto);
    }

    /**
     * Ajoute un score au Sudoku
     * @param pseudo Pseudo du joueur
     * @param score Score du joueur
     */
    public void ajouterScoreSudoku(String pseudo, Integer score){
        scoresSudoku.add(toPair(pseudo,score));
        Collections.sort(scoresSudoku);
    }

    /**
     * Ajoute un score à la Bataille navale
     * @param pseudo Pseudo du joueur
     * @param score Score du joueur
     */
    public void ajouterScoreNavale(String pseudo, Integer score){
        scoresNavale.add(toPair(pseudo,score));
        Collections.sort(scoresNavale);
    }

    /**
     * Ajoute un score au Poker
     * @param pseudo Pseudo du joueur
     * @param score Score du joueur
     */
    public void ajouterScorePoker(String pseudo, Integer score){
        scoresPoker.add(toPair(pseudo,score));
        Collections.sort(scoresPoker);
    }

    /**
     * Retourne une paire de pseudo/score
     * @param pseudo Pseudo du joueur
     * @param score Score du joueur
     * @return Pair de pseudo/score
     */
    private Pair<String,Integer> toPair(String pseudo, Integer score){
        return new Pair<>(pseudo, score);
    }

    public List<Pair<String, Integer>> getScoresLoto() { return scoresLoto; }

    public List<Pair<String, Integer>> getScoresSudoku() { return scoresSudoku; }

    public List<Pair<String, Integer>> getScoresNavale() {
        return scoresNavale;
    }

    public List<Pair<String, Integer>> getScoresPoker() {
        return scoresPoker;
    }
}
