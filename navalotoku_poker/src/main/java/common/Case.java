package common;

import java.io.Serializable;

public class Case<T> implements Serializable {
    private T val;

    /**
     * Constructeur du type Case
     * @param val Valeur de la case
     */
    public Case(T val){
        this.setVal(val);
    }

    /**
     * Récupère la valeur de la case
     * @return Valeur de la case
     */
    public T getVal() {
        return val;
    }

    /**
     * Change la valeur de la case
     * @param val Valeur
     */
    public void setVal(T val) {
        this.val = val;
    }

    public String toString(){
        return val.toString();
    }
}