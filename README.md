# Navalotoku Poker

## Introduction

Ce recueil de jeux a été réalisé dans le cadre d'un projet à l'Univesité Polytechnique des Hauts-de-France.

## Installation

### Prérequis

Voici ce dont vous aurez besoin pour compiler le projet : 

    - Java Developpement Kit 13
    - Apache Maven

### Compilation

Allez dans le dossier "navalotoku_poker" puis tapez la commande suivante :

```sh
mvn clean javafx:compile javafx:jlink
```

### Lancement

Il suffit d'entrer la commande :

```sh
./target/image/bin/launcher
```
sous Mac et Linux.
```sh
./target/image/bin/launcher.bat
```
sous Windows.

## Téléchargement

[Windows](https://www.dropbox.com/s/rt5jyq2j0nuz1gj/Navalotoku_Poker_Windows.zip?dl=1)  
[Mac](https://www.dropbox.com/s/f78o2ncxpy8p1oi/Navalotoku_Poker_Mac.zip?dl=1)  
[Linux](https://www.dropbox.com/s/kwfkpdxyl2w6kqt/Navalotoku_Poker_Linux.zip?dl=1)  
[Javadoc](https://www.dropbox.com/s/ndmt4zwkt86m23b/Navalotoku_Poker_Javadoc.zip?dl=1)

### Lancement

Il suffit finalement d'entrer dans le dossier image et d'entrer la commande :

```sh
./bin/launcher
```
sous Mac et Linux.
```sh
./bin/launcher.bat
```
sous Windows.

## Contributeurs

* **Hugo Haquette**
* **Anthéa Tibi**
* **Pascal Carlier**
